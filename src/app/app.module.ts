import {NgModule} from '@angular/core';
import {SwiperModule} from 'swiper/angular';
import {AppComponent} from './app.component';
import {RouteReuseStrategy} from '@angular/router';
import {AppRoutingModule} from './app-routing.module';
import {BrowserModule} from '@angular/platform-browser';
import {IonicStorageModule} from '@ionic/storage-angular';
import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {WelcomeScreenComponent} from './welcome-screen/welcome-screen.component';

@NgModule({
  declarations: [AppComponent, WelcomeScreenComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    IonicStorageModule.forRoot(),
    SwiperModule
  ],
  providers: [{provide: RouteReuseStrategy, useClass: IonicRouteStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule {}
