import {Component, OnDestroy, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {GardenEditComponent} from './garden-edit/garden-edit.component';
import {StorageService} from '../storage.service';
import {Garden} from '../shared/garden.model';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit, OnDestroy {
  gardens: Garden[];
  gardenSelected: string;
  gardenSelectedSubscribe;
  currentYear: number = new Date().getFullYear();
  yearSelected: number = this.currentYear;
  isModalOpen = false;
  gardenSubscribe;

  constructor(public modalController: ModalController,
              private storageService: StorageService) {
  }

  async ngOnInit() {
    this.gardens = await this.storageService.getGardens().then((value => {
      if (value !== null) {
        this.gardenSelected = value[0].name;
      }
      return value;
    }));
    console.log('gardens', this.gardens);
    this.gardenSubscribe = this.storageService.gardensUpdated.subscribe(
      (gardens: Garden[]) => {
        // console.log('gardens updated (subscribe)');
        this.gardens = gardens;
      }
    );

    this.gardenSelectedSubscribe = this.storageService.gardenSelected.subscribe((name) => {
      this.gardenSelected = name;
      console.log('Garden selected : ' + name);
    });
  }

  async onAddGarden() {
    const modal = await this.modalController.create({
      component: GardenEditComponent,
      componentProps: {
        editMode: false
      }
    });
    return await modal.present();
  }

  async onEditGarden() {
    const modal = await this.modalController.create({
      component: GardenEditComponent,
      componentProps: {
        editMode: true,
        id: this.gardenSelected
      }
    });
    return await modal.present();
  }

  onDeleteGarden() {
    this.storageService.deleteGarden(this.gardenSelected);
  }

  onChangeGarden() {
    this.storageService.gardenSelected.next(this.gardenSelected);
  }

  onChangeYear(value) {
    this.yearSelected = new Date(value).getFullYear();
  }

  deleteTable() {
    this.storageService.deleteGardens();
  }

  ngOnDestroy() {
    this.gardenSubscribe.unsubscribe();
    this.gardenSelectedSubscribe.unsubscribe();
  }
}
