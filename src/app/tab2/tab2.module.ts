import {Tab2Page} from './tab2.page';
import {NgModule} from '@angular/core';
import {IonicModule} from '@ionic/angular';
import {CommonModule} from '@angular/common';
import {Tab2PageRoutingModule} from './tab2-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {GardenEditComponent} from './garden-edit/garden-edit.component';
import {GardenCanvasComponent} from './garden-canvas/garden-canvas.component';
import {ExploreContainerComponentModule} from '../explore-container/explore-container.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FormsModule,
    ExploreContainerComponentModule,
    Tab2PageRoutingModule
  ],
  declarations: [
    Tab2Page,
    GardenCanvasComponent,
    GardenEditComponent
  ]
})
export class Tab2PageModule {}
