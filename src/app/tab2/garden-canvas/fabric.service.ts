import {Injectable} from '@angular/core';
import {StorageService} from '../../storage.service';
import {fabric} from 'fabric';
import {Garden} from '../../shared/garden.model';
import {Image} from 'fabric/fabric-impl';
import {VegetablesService} from '../../shared/vegetables.service';
import {Vegetable} from '../../shared/vegetable.model';

@Injectable({providedIn: 'root'})
export class FabricService {
  canvas: any;
  document: any;
  defaultScale = 0.1;

  constructor(
    private storageService: StorageService,
    private vegetableService: VegetablesService) {
  }

  initFabric(document: Document) {
    this.document = document;
    const height = document.getElementById('canvasContainer').offsetHeight;
    this.canvas = new fabric.Canvas('canvas', {
      width: window.innerWidth,
      height,
      allowTouchScrolling: true,
      selection: false,
    });

    fabric.Object.prototype.set({
      snapThreshold: 0,
      snapAngle: 18,
      lockScalingFlip: true,
      strokeWidth: 2,
      strokeUniform: true,
      transparentCorners: true,
      cornerColor: '#22A7F0',
      borderColor: '#22A7F0',
      cornerSize: 12,
      padding: 5,
      originX: 'center', originY: 'center'
    });
    this._initControls();
    this.canvas.setDimensions({width: '100%', height: '100%'}, {cssOnly: true});
    return this.canvas;
  }

  /**
   * Add rectangle to the view.
   */
  addRect() {
    // create a rect object
    const rect = new fabric.Rect({
      left: 0, top: 0,
      fill: '#D81B60',
      width: 200,
      height: 200,
      objectCaching: false,
      stroke: '"#880E4F',
    });
    this._bindImage(rect);
  }

  /**
   * Add circle to the view.
   */
  addCircle() {
    const circle = new fabric.Circle({
      top: 0, left: 0,
      radius: 75,
      fill: '#D81B60',
      stroke: '"#880E4F',
      lockRotation: true,
    });
    this._bindImage(circle);
  }

  /**
   * Add triangle.
   */
  addTriangle() {
    const triangle = new fabric.Triangle({
      left: 0, top: 0,
      fill: '#D81B60',
      width: 200,
      height: 100,
      objectCaching: false,
      stroke: '"#880E4F',
    });
    this._bindImage(triangle);
  }

  /**
   * Add text to the view.
   */
  addText() {
    const txt = new fabric.IText('Tapez votre texte ici', {
      left: 200, top: 100,
      fill: 'black',
      fontFamily: 'Delicious_500',
      selectable: true,
      lockScalingFlip: true,
      fontSize: 25,
    });
    this.canvas.add(txt);
    this.canvas.setActiveObject(txt);
    this.canvas.requestRenderAll();
  }

  /**
   * Unlock the garden map.
   */
  unlockEdit() {
    this.canvas.selection = true;
    const objects = this.canvas.getObjects();
    for (const item of objects) {
      item.set({
        hasControls: true,
        selectable: true,
      });
    }
  }

  /**
   * Discard active object and set selection to false.
   */
  lockEdit() {
    this.canvas.selection = false;
    this.canvas.discardActiveObject();
    const objects = this.canvas.getObjects();
    for (const item of objects) {
      item.set({
        hasControls: false,
        selectable: false,
      });
    }
    this.canvas.requestRenderAll();
  }

  /**
   * Save garden into local storage.
   * @param gardenName  The garden ID
   * @param year The year of the garden map
   */
  saveGardenDraw(gardenName: string, year: number) {
    const gardenJSON = JSON.stringify(this.canvas.toJSON());
    this.storageService.updateDraw(gardenName, gardenJSON, year).then();
  }

  /**
   * Restore the garden map form JSON stored in the local storage then lock edit.
   * @param gardenName The garden ID
   * @param year The year to restore
   */
  restoreFromJSON(gardenName: string, year: number) {
    this.canvas.clear(); // Reset canvas
    if (gardenName === null) {
      return;
    }
    this.storageService.getGarden(gardenName).then((garden: Garden) => {
      if (Object.keys(garden.maps).length !== 0) {
        this.canvas.loadFromJSON(garden.maps[year], () => {
          this.lockEdit();
          this.initObjects(); // Restore features
        });
      }
    });
  }

  /**
   * Allow to delete an element.
   * @private
   */
  private initObjects() {
    this._initControls();
    this.canvas.on('object:scaling', () => {
      this._updateScale();
    });
    this.canvas.on('selection:cleared', () => {
      this._updateScale();
    });
    const objects = this.canvas.getObjects();
    for (const item of objects) {
      item.on('rotating', () => {
        this._updateRotation(item);
      });
    }
  }

  /**
   * Add objects controls
   * @private
   */
  private _initControls() {
    const deleteIcon = 'data:image/svg+xml,%3C%3Fxml version=\'1.0\' encoding=\'utf-8\'%3F%3E%3C!DOCTYPE svg PUBLIC \'-//W3C//DTD SVG 1.1//EN\' \'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\'%3E%3Csvg version=\'1.1\' id=\'Ebene_1\' xmlns=\'http://www.w3.org/2000/svg\' xmlns:xlink=\'http://www.w3.org/1999/xlink\' x=\'0px\' y=\'0px\' width=\'595.275px\' height=\'595.275px\' viewBox=\'200 215 230 470\' xml:space=\'preserve\'%3E%3Ccircle style=\'fill:%23F44336;\' cx=\'299.76\' cy=\'439.067\' r=\'218.516\'/%3E%3Cg%3E%3Crect x=\'267.162\' y=\'307.978\' transform=\'matrix(0.7071 -0.7071 0.7071 0.7071 -222.6202 340.6915)\' style=\'fill:white;\' width=\'65.545\' height=\'262.18\'/%3E%3Crect x=\'266.988\' y=\'308.153\' transform=\'matrix(0.7071 0.7071 -0.7071 0.7071 398.3889 -83.3116)\' style=\'fill:white;\' width=\'65.544\' height=\'262.179\'/%3E%3C/g%3E%3C/svg%3E';
    const img = document.createElement('img');
    img.src = deleteIcon;
    fabric.Object.prototype.controls.deleteControl = new fabric.Control({
      x: 0,
      y: -0.5,
      offsetY: -30,
      offsetX: 40,
      cursorStyle: 'pointer',
      mouseUpHandler: (): boolean => {
        const activeObjects = this.canvas.getActiveObjects();
        if (activeObjects) {
          for (const activeObject of activeObjects) {
            this.canvas.remove(activeObject);
          }
          this.canvas.discardActiveObject();
          this.canvas.renderAll();
        } else {
          this.canvas.getActiveObject().remove();
        }
        return true;
      },
      render: (ctx, left, top, styleOverride, fabricObject) => {
        const size = 24;
        ctx.save();
        ctx.translate(left, top);
        ctx.rotate(fabric.util.degreesToRadians(fabricObject.angle));
        ctx.drawImage(img, -size / 2, -size / 2, size, size);
        ctx.restore();
      },
    });
  }

  /**
   * Update the img scale when on resize.
   * @private
   */
  private _updateScale() {
    const objects = this.canvas.getObjects();
    for (const object of objects) {
      if (
        object.getObjects()[0].type && (
          object.getObjects()[0].type === 'circle' ||
          object.getObjects()[0].type === 'rect' ||
          object.getObjects()[0].type === 'triangle')) {

        if (object.scaleX < 1) { // True when the width is too small compared to the height
          object.getObjects()[1].scale(0.2);
          object.getObjects()[1].scaleY = object.scaleX / object.scaleY * 0.2;
        }
        // True when the height is too small compared to the width
        else if (object.scaleY < 1) {
          object.getObjects()[1].scale(0.2);
          object.getObjects()[1].scaleX = object.scaleY / object.scaleX * 0.2;
        } else {
          object.getObjects()[1].scaleX = object.scaleY / object.scaleX * this.defaultScale;
          object.getObjects()[1].scaleY = this.defaultScale; // Reset Y; in case if the object matched the condition: 1
        }
      }
    }
  }

  private _updateRotation(group: fabric.Group) { // Need support !
    const newAngle = -group.angle;
    if (group.angle === 0 || group.angle === 180) {
      group.getObjects()[1].rotate(newAngle);
    }
  }

  /**
   * Bind the vegetable image into to object (Rect, Triangle, Circle).
   * @param object the object to bind with the image
   * @private
   */
  private _bindImage(object: fabric.Rect | fabric.Circle | fabric.Triangle) {
    fabric.Image.fromURL('../../../assets/vegetables/celery.png', (img: Image) => {
      const img1 = img.scale(this.defaultScale).set({left: 0, top: 0});

      const group = new fabric.Group([object, img1], {left: 200, top: 200});
      this.canvas.add(group);
      group.sendToBack();
      this.canvas.setActiveObject(group).requestRenderAll();
      this.initObjects(); // Add group features
    });
  }

  /**
   * Changes the image according to the name given as a parameter.
   * @param group The group which contain the img
   * @param newVegetable The new vegetable name (Vegetable model)
   * @private
   */
  private _updateImage(group: fabric.Group, newVegetable: string) {
    return new Promise<void>((resolve, reject) => {
      const imageObject: any = group.getObjects()[1];
      const vegetable: Vegetable | null = this.vegetableService.getVegetable(newVegetable);

      if (vegetable == null && typeof (imageObject) !== 'undefined') {
        return reject();
      }
      imageObject.setSrc(vegetable.imagePath);
      return resolve();
    });
  }
}
