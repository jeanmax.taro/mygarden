import {Injectable} from '@angular/core';
import {Garden} from './shared/garden.model';
import {Subject} from 'rxjs';
import {Storage} from '@ionic/storage-angular';
import {ToastNotificationService} from './toast-notification.service';

@Injectable({providedIn: 'root'})
/**
 * Garden manager service
 * @link https://github.com/ionic-team/ionic-storage
 * @author DERACHE Adrien
 */
export class StorageService {
  dataBaseName = 'data.db';
  gardensUpdated = new Subject<Garden[]>();
  gardenSelected = new Subject<string>();

  constructor(private storage: Storage,
              private toastNotificationService: ToastNotificationService) {
    this.createDataBase().then();
  }

  /**
   * Return true if the app is launched for the first time.
   */
  async isFirstLaunch(): Promise<boolean> {
    return await this.storage.get('firstTime').then(async res => {
      if (res === null || res === 'true') {
        return true;
      } else if (res === 'false') {
        return false;
      }
    });
  }

  /**
   * Set firstTime to false in local storage.
   */
  async unLockApp() {
    await this.storage.set('firstTime', 'false');
  }

  /**
   * Outputs the gardens
   * @return Garden[]
   */
  async getGardens(): Promise<Garden[]> {
    return new Promise<Garden[]>(async (resolve) => {
      const gardens: Garden[] = [];
      const storageLength = await this.storage.length();
      if (storageLength - 1 === -1) {
        resolve(null);
      }
      await this.storage.forEach((value, key, index) => {
        const garden: Garden | null = (JSON.parse(value).name) ? JSON.parse(value) : null;
        if (garden !== null) {
          gardens.push(
            {
              name: garden.name,
              description: garden.description,
              vegetable: garden.vegetable,
              maps: garden.maps
            });
        }
        if (storageLength === index) {
          if (gardens.length <= 0) {
            resolve(null);
          }
          resolve(gardens);
        }
      });
    });
  }

  /**
   * Get specific garden
   * @param name the garden id
   * @return Garden
   */
  async getGarden(name): Promise<Garden> {
    return new Promise<Garden>(async (resolve) => {
      const garden = JSON.parse(await this.storage.get(name));
      console.log('Garden obtained', garden, ' with (key): ', name);
      resolve(garden);
    });
  }

  /**
   * Add a garden into local storage
   * @param garden Garden object to save
   */
  async addGarden(garden: Garden) {
    console.log(garden);
    const name = garden.name;
    const description = garden.description;
    const vegetable = garden.vegetable;
    const maps = 'maps' in garden ? garden.maps : {};
    await this.storage.set(name, JSON.stringify({name, description, vegetable, maps})).then(async () => {
      await this.toastNotificationService.presentToast({text: 'Jardin sauvegardé avec succès !'});
      this.gardensUpdated.next(await this.getGardens());
      this.gardenSelected.next(name);
      // tslint:disable-next-line:no-console
      console.info('Garden: ' + name + ' saved !');
    });
  }

  /**
   * Update the garden map.
   * @param name garden ID
   * @param map The fabric JSON
   * @param year The year of the garden map
   */
  async updateDraw(name: string, map: string, year: number) {
    const gardenMap = JSON.parse(map);
    await this.getGarden(name).then(async (garden: Garden) => {
        garden.maps[year] = gardenMap;
        await this.storage.set(name, JSON.stringify(garden));
      }
    );
  }

  /**
   * Update a garden NOT THE MAP but keep it !.
   * @param oldGardenName old garden ID
   * @param newGarden new garden object
   */
  async updateGarden(oldGardenName: string, newGarden: Garden) { // Remove the old garden and create new with new properties
    // Get the map if exists
    if (!('maps' in newGarden)) {
      await this.getGarden(oldGardenName).then((garden: Garden) => {
        if ('maps' in garden) {
          newGarden.maps = garden.maps;
        }
      });
    }
    await this.deleteGarden(oldGardenName).then(() => {
      this.addGarden(newGarden);
    });
  }

  /**
   * Delete all the gardens
   */
  async deleteGardens() {
    await this.storage.clear();
    this.gardensUpdated.next(await this.getGardens());
    this.gardenSelected.next(null);
    // tslint:disable-next-line:no-console
    console.info('Gardens deleted !');
  }

  /**
   * Delete specific garden
   * @param name garden ID
   */
  async deleteGarden(name: string) {
    await this.storage.remove(name).then(async () => {
      this.gardensUpdated.next(await this.getGardens());
      await this.toastNotificationService.presentToast({text: 'Jardin supprimé avec succès !'});
      this.getGardens().then((gardens) => {
        if (gardens === null) {
          this.gardenSelected.next(null);
        } else {
          this.gardenSelected.next(gardens[0].name);
        }
      });
      // tslint:disable-next-line:no-console
      console.info('Garden: ' + name + ' deleted');
    }, (error) => {
      console.error(error);
    });
  }

  /**
   * Init database
   * @private
   */
  private async createDataBase() {
    await this.storage.create().then(() => {
      console.log('Storage successfully created !');
    }, (error) => {
      console.error(error);
    });
  }
}
