import {Component, ViewChild, ViewEncapsulation} from '@angular/core';
import {StorageService} from '../storage.service';
import {Router} from '@angular/router';
import {SwiperComponent} from 'swiper/angular';
import SwiperCore, {Pagination, SwiperOptions} from 'swiper';

SwiperCore.use([Pagination]);

@Component({
  selector: 'app-welcome-screen',
  templateUrl: 'welcome-screen.component.html',
  styleUrls: ['welcome-screen.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WelcomeScreenComponent {
  @ViewChild('swiper') swiper: SwiperComponent;

  config: SwiperOptions = {
    pagination: true,
  };

  constructor(private storageService: StorageService, private router: Router) {
  }

  start() {
    this.storageService.unLockApp().then(() => {
      this.router.navigate(['/tabs', 'tab1']);
    });
  }

}
