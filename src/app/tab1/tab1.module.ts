import {IonicModule} from '@ionic/angular';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Tab1Page} from './tab1.page';
import {ExploreContainerComponentModule} from '../explore-container/explore-container.module';

import {Tab1PageRoutingModule} from './tab1-routing.module';
import {VegetablesListComponent} from './vegetables-list/vegetables-list.component';
import {VegetableItemComponent} from './vegetables-list/vegetable-item/vegetable-item.component';
import {DepositModalComponent} from './deposit-modal/deposit-modal.component';
import {GeneralComponent} from './deposit-modal/general/general.component';
import {InGardenComponent} from './deposit-modal/in-garden/in-garden.component';
import {AboutComponent} from './deposit-modal/about/about.component';
import {ShortenPipe} from './shorten.pipe';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    Tab1PageRoutingModule
  ],
  declarations: [
    Tab1Page,
    VegetablesListComponent,
    VegetableItemComponent,
    DepositModalComponent,
    GeneralComponent,
    InGardenComponent,
    AboutComponent,
    ShortenPipe
  ]
})
export class Tab1PageModule {}
