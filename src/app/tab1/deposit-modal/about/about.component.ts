import {Component, Input, OnInit} from '@angular/core';
import {Vegetable} from '../../../shared/vegetable.model';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
})
export class AboutComponent implements OnInit {

  @Input() vegetable: Vegetable;

  constructor() { }

  ngOnInit() {}

}
