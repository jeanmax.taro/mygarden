/**
 * Garden model use for tge local storage
 * @author DERACHE Adrien
 */
export class Garden {

  public name: string;
  public description: string;
  public vegetable: string[];
  public maps: { ['year']: {} };

  constructor(name: string, description: string, vegetable?: string[], maps?: { ['year']: {} }) {
    this.name = name;
    this.description = description;
    this.vegetable = vegetable;
    this.maps = maps;
  }
}
