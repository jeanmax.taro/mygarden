import {Vegetable} from './vegetable.model';
import {Injectable} from '@angular/core';

@Injectable({providedIn: 'root'})
export class VegetablesService {
  vegetables: Vegetable[] = [

    new Vegetable(
      'Tomate',
      'Solanaceae',
      'Très simple à consommer, elle se prête à une infinité de préparations. Très riche au niveau nutritionnel, elle a de véritables atouts bien-être. Toutes ses qualités en font le légume le plus consommé en France.',
      [3, 10],
      '3 à 4 jours à l\'air ambiant',
      '20 kcal pour 100g',
      'Riche en fibres et vitamine C',
      ['Basilic', 'Origan', 'Persil', 'Ciboulette', 'Oignon', 'Capucine', 'Célerie', 'Carotte', 'Boufrache', 'Géranium'],
      ['Pomme de terre', 'Fenouil', 'Famille du chou','Betteraves', 'Maïs'],
      'Découverte en Amérique du Sud au XVIe siècle, la tomate tient son nom d’un mot aztèque : « Tomatl ». Après avoir traversé l’Océan, elle parvient dans le sud de l’Europe. Les Italiens l’appelaient alors « pomme d’or » et les Provençaux « pomme d’amour ». Considérée comme vénéneuse, ses plants étaient cultivés pour leurs qualités ornementales. L’agronome Olivier de Serres la préconisait pour la décoration de tonnelles. Ses propriétés en tant que légume fruit ne furent découvertes qu’au milieu du XVIIIe siècle. Elle gagna alors toutes les tables de l’Europe méridionale. En France, les Provençaux furent les premiers à la consommer. Elle était préparée sous forme de sauces plus ou moins relevées.',
      '../../../assets/vegetables/tomato.png'
    ),

    new Vegetable(
      'Carotte',
      'Apiaceae',
      'Elle renferme de très nombreux minéraux et vitamines. D’après l’adage, elle rendrait même aimable...',
      [8, 3], // [8, 3],
      '8 jours maximum dans le bac à légumes du réfrigérateur',
      '40 kcal pour 100g',
      'Riche en fibres et vitamine B9',
      ['Laitue', 'Ciboulette', 'Poireau', 'Romarin', 'Pois', 'Armoise', 'Concombre','Haricots','Oignon','Radis', 'Sauge', 'Tomates'],
      ['Fraise', 'Fenouil', 'Chou','Persil'],
      'Les habitants du bassin méditerranéen ont commencé à consommer la carotte bien longtemps avant notre ère. Mais Grecs et Romains ne semblaient guère l’apprécier. Il faut dire qu’à cette époque, les carottes avaient une couleur blanchâtre, une peau assez coriace, et un cœur très fibreux. À la Renaissance, on parviendra à améliorer l’espèce et à rendre la carotte plus savoureuse. Mais ce n’est qu’à partir du milieu du 19e siècle que la carotte va acquérir sa belle couleur rouge orangé, et devenir enfin le tendre légume que nous connaissons.',
      '../../../assets/vegetables/carrot.png'
    ),

    new Vegetable(
      'Aubergine',
      'Solanaceae',
      'La plus fréquente sur les étals est l’aubergine violette, à la chair blanche et moelleuse.',
      [7, 9],
      'Jusqu’à 5 jours dans le bac à légumes',
      '33 kcal pour 100g',
      'Riche en fibres et vitamine B5',
      ['Pois', 'Menthe', 'Souci', 'Calendula','Artichaut','Fève', 'Haricot', 'Lavande','Persil', 'Radis'],
      ['Ail','Ciboulette', 'Oignon', 'Piment', 'Pois', 'Poivron', 'Pomme de terre', 'Tomate.'],
      '\n' +
      '\n' +
      'La culture de l’aubergine est très ancienne. Elle remonte en effet à 800 ans avant J.-C., dans la région indo-birmane. Le légume prospère toujours en terre indienne qui dispose d’une quantité impressionnante de variétés de toutes les couleurs. On la trouve également en Chine, quatre siècles avant notre ère.\n' +
      '\n' +
      'L’aubergine est ensuite implantée dans le bassin méditerranéen grâce aux navigateurs arabes qui la ramènent de leurs voyages du bout du monde. Le légume s’acclimate rapidement à la douce chaleur de la région, et prospère rapidement. \n' +
      '\n' +
      'Petit à petit, l’aubergine fait ses premiers pas sur le continent européen, cultivée par les Espagnols au Moyen-Âge. Il faudra attendre le XVe siècle pour qu’elle se développe en Italie et dans le sud de France.\n',
      '../../../assets/vegetables/aubergine.png'
    ),

    new Vegetable(
      'Oignon',
      'Liliaceae',
      'S’invitant dans un nombre impressionnant de préparations, il se prête avec autant de réussite aux plats les plus rustiques (flamiche, gratinée…) qu’aux mets les plus sophistiqués (foie gras poêlé, magret de canard…). Ce bulbe, organe de réserves nutritives pour la plante et sa fleur, présente aussi des qualités nutritionnelles qui vous invitent à conjuguer bien-être et plaisir de la table.',
      [9, 4],
      '3 ou 4 jours dans le bac à légumes',
      '21 kcal pour 100g',
      'Riche en fibres et vitamine C',
      ['Carotte','Blette','Betterave','Camomille', 'Carotte', 'Céleri-branche','Céleri-rave','Concombre', 'Cornichon', 'Fraise', 'Laitue','Mâche','Navet','Tomate','Épinards','Panais', 'Persil',],
      ['Ail', 'Asperge', 'Aubergine', 'Brocoli', 'Chou', 'Chou de Bruxelles', 'Chou-fleur', 'Ciboulette', 'Echalote', 'Fève', 'Haricot', 'Piment', 'Poireau', 'Pois', 'Poivron', 'Pomme de terre', 'Sauge', 'Tomate'],
      'Si on ne connaît pas l’ancêtre sauvage de l’oignon, on sait en revanche qu’il est cultivé depuis plus de 5000 ans. Les premières traces de production viennent de Mésopotamie, sa culture gagnant ensuite l’Egypte antique (des textes datant de plus de 4 000 ans le mentionnent), la Grèce, l’Empire romain puis le reste de l’Europe et du Monde. Il est même symbole de l’intelligence dans la Chine ancienne.      \n' +
      '\n' +
      'Incontournable au Moyen-âge\n' +
      '\n' +
      'Si le terme « ognon » n’apparait dans la langue française qu’en 1273, il est consommé abondamment au Moyen-âge, dont les croyances et superstitions lui prêtent certains pouvoirs (comme calmer la douleur en cataplasme par exemple). Il constitue surtout l’un des piliers de la cuisine et de l’alimentation et rares sont les potagers où ne poussent pas quelques oignons !         \n' +
      '\n' +
      'Découverte de l’Amérique et conquête du monde\n' +
      '\n' +
      'L’oignon est une des premières plantes européennes à être cultivées en Amérique, où il est introduit par Christophe Colomb. On le rencontre aujourd’hui sur toute la surface du globe.',
      '../../../assets/vegetables/onion.png'
    ),

    new Vegetable(
      'Poire',
      'Rosaceae',
      'Disponible toute l’année, elle se décline en une dizaine de variétés qui se répartissent entre l’été et la saison d’automne-hiver. Peu calorique, elle renferme de nombreux nutriments et des fibres. Dégustez-la à la croque ou dévoilez toute sa douceur dans de nombreux desserts ou dans des recettes salées. Adoptez-la, elle ne vous veut que du bien !',
      [7, 4],
      '2 à 3 jours à l’air ambiant',
      '53 kcal pour 100g',
      'Riche en vitamine B9',
      ['Non renseigné'],
      ['Non renseigné'],
      '\n' +
      '\n' +
      'Petit à petit, l’Europe fait sa connaissance. En France, elle apparaît au XVIe siècle et le roi Louis XIV la popularise grâce à son jardinier La Quintinie. Plusieurs espèces de poiriers furent cultivées dans le célèbre « potager du roi ».\n' +
      '\n' +
      'Au fil des siècles, de multiples variétés sont sélectionnées et retenues. En France, on en produit actuellement une dizaine. L’une des plus populaires est la Conférence, née en Angleterre. Elle doit son nom au prix qu’elle a remporté à la conférence nationale britannique de la poire en 1885.\n',
      '../../../assets/vegetables/pear.png'
    ),

    new Vegetable(
      'Pomme de terre',
      'Solanaceae',
      'La culture de la pomme de terre recouvre une quantité de variétés exceptionnelle. Vous trouverez sur les étals près de 320 variétés dont une vingtaine prédominantes.',
      [9, 3],
      'Indéterminé',
      'Non renseigné',
      'Riche en vitamine B6',
      ['Brocoli', 'Capucine', 'Chou pommé', 'Chou de Bruxelles', 'Chou-fleur', 'Coriandre',  'Laitue', 'Oeillet d’Inde', 'Souci', 'Topinambour'],
      ['Aubergine', 'Blette', 'Betterave', 'Carotte', 'Céleri', 'Concombre', 'Cornichon', 'Courge', 'Courgette', 'Echalote', 'Epinard', 'Fève', 'Framboise', 'Haricot vert', 'Maïs', 'Melon', 'Navet', 'Oignon', 'Piment', 'Pois', 'Poivron', 'Radis',  'Tomate', 'Tournesol'],
      'Née il y a 8 000 ans sur les plateaux de la Cordillère des Andes, la pomme de terre fut rapidement cultivée par les Incas dès le XIIIe siècle.\n' +
      '\n' +
      'Il faudra attendre 3 siècles pour qu’elle atteigne les rivages européens, grâce aux conquistadors espagnols. Le légume migre ensuite vers l’Italie, puis en Allemagne et dans le sud de la France. Elle commence à être cultivée sous l’impulsion d’Olivier de Serres et de Charles de l’Escluze.\n' +
      '\n' +
      'De la soue à nos assiettes\n' +
      '\n' +
      'Longtemps boudée par les consommateurs français, la pomme de terre est réservée en premier lieu à la nourriture des cochons et des animaux de la ferme. Ce n’est qu’au XVIIIe siècle que le pharmacien Parmentier arrive à séduire les palais français. Solution à la famine, vertus nutritives et gustatives, la pomme de terre est auréolée de vertus et s’invite sur toutes les tables au XIXe siècle.',
      '../../../assets/vegetables/potato.png'
    ),

    new Vegetable(
      'Epinard',
      'Chenopodiaceae',
      'Reconnu pour ses qualités nutritionnelles et ses bienfaits énergétiques, il entre dans la composition de plats savoureux. Aliment bien-être, il vaut le coup d’être découvert ou redécouvert pour sa saveur !',
      [9, 6],
      'Pas plus de 2 jours au réfrigérateur',
      '28 kcal pour 100g',
      'Riche en fibres et vitamine K1',
      ['Ail', 'Asperge', 'Aubergine', 'Brocoli',  'Camomille', 'Carotte', 'Céleri', 'Chou', 'Chou de Bruxelles', 'Chou-fleur', 'Concombre', 'Fève', 'Fraise', 'Haricot',  'Maïs',  'Melon', 'Navet', 'Oignon', 'Poireau', 'pois', 'Radis', 'Souci'],
      ['Blette', 'Betterave', 'Laitue', 'Piment', 'Poivron', 'Pomme de terre', 'Tomate'],
      '\n' +
      '\n' +
      'Très présent dans les cultures arabes, l’épinard était utilisé en cataplasmes dans le traitement des douleurs du foie et de l’estomac. Il semblerait que Grecs et Romains n’aient pas connu ces feuilles vertes à l’Antiquité.\n' +
      '\n' +
      'C’est donc au Moyen Âge qu’il atteint la France, ramené de Jérusalem par les Croisés ou provenant de la péninsule Ibérique alors sous domination arabe. Les Européens ne lui prêtent alors que peu d’attention et le considèrent même comme « l’herbe du carême » : en consommer est en soi une privation !\n' +
      '\n' +
      'Il fallut attendre la gourmande Catherine de Médicis pour qu’il soit reconnu et cultivé. De nos jours encore, on nomme « à la florentine », nombre de mets à base d’épinards.\n',
      '../../../assets/vegetables/spinach.png'
    ),


    new Vegetable(
      'Laitue',
      'Asteraceae',
      'La Lactuca sativa regroupe plusieurs centaines de variétés ; guère étonnant puisqu’elle est cultivée depuis la plus lointaine Antiquité. Croquante ou fondante, charnue ou légère, elle se déguste surtout en salade, mais se prête à bon nombre de préparations cuites.',
      [5, 9],
      '1 à 2 jours dans le bac à légumes',
      '14 kcal pour 100g',
      'Riche en fibres et vitamine K1',
      ['Ail', 'Asperge', 'Blette', 'Betterave', 'Carotte', 'Cerfeuil', 'Chou', 'Chou de Bruxelles', 'Chou-fleur', 'Concombre', 'Cornichon', 'Echalote', 'Fève', 'Fraise', 'Haricot', 'Mâche', 'Melon', 'Navet', 'Oignon', 'Poireau', 'Pois', 'Potiron', 'Radis', 'Souci'],
      ['Artichaut', 'Brocoli', 'Céleri', 'Endive', 'Epinard', 'Maïs', 'Persil', 'Tournesol'],
      '\n' +
      '\n' +
      '    Les Grecs et les Romains cultivent des laitues de type celtique ou romaine non pommée. Ils la consomment notamment pour préparer leur estomac à leurs copieux repas.\n' +
      '    La laitue est une des 88 plantes citées dans le Capitulaire De Villis de Charlemagne, attestant de sa consommation durant le Haut Moyen-Âge.\n' +
      '    A la Renaissance, encore peu de variétés sont cultivées, on en recense trois ou quatre en France, et huit en Angleterre.\n' +
      '    C’est au cours des XVIIe et XVIIIe siècles qu’apparait la gamme actuelle des laitues pommées, en même temps qu’augmente le nombre des variétés. Cet essor s’explique notamment par la pratique du forçage (visant à faire pousser des plantes en dehors de leur saison normale de croissance.)\n' +
      '    Au cours du XIXe siècle, la culture s’amplifie et la salade apparait dans toutes les ceintures vertes maraîchères.\n' +
      '\n' +
      'Depuis les années 1960, on constate un nouvel essor de la production, s’expliquant par les cultures en serre et sous tunnel plastique.\n',
      '../../../assets/vegetables/lettuce.png'
    ),

    new Vegetable(
      'Radis',
      'Brassicaceae',
      'Longues, rondes, petites ou grosses, les différentes variétés proposent une subtile palette de saveurs. Croqué au naturel avec un peu de sel, c’est un pur aliment plaisir reconnu aussi pour ses qualités nutritionnelles. ',
      [3, 6],
      'Le jour même ',
      '14 kcal pour 100g',
      'Riche en fibres et vitamine B9',
      ['Artichaut', 'Blette', 'Betterave', 'Capucine', 'Carotte', 'Céleri-branche', 'Céleri-rave', 'Cerfeuil', 'Cresson', 'Endive', 'Epinard', 'Fève', 'Haricot', 'Laitue', 'Melon',  'Menthe', 'Panais', 'Persil', 'Pois', 'Souci', 'Tomate'],
      ['Brocoli', 'Chou pommé', 'Chou-fleur', 'Chou de Bruxelles',  'Concombre', 'Cornichon', 'Courge', 'Courgette', 'Pomme de terre', 'Rutabaga', 'Raisin'],
      'Probablement originaire d’Asie Mineure, le radis faisait déjà partie du menu des Babyloniens et des Égyptiens il y a 4 000 ans.\n' +
      'Chez les Grecs, le radis était dédié à Apollon. Le dieu recevait parfois des radis en or comme offrandes.\n' +
      'Charlemagne, dans ses fameux capitulaires, recommandait à ses sujets la culture de l’excellente racine.\n' +
      'Déjà consommé « croque au sel », le radis du Moyen Âge n’avait probablement pas grand-chose à voir avec les variétés que nous consommons aujourd’hui.\n' +
      'Le radis noir n’est arrivé en France qu’au XVIe siècle : il ne sera consommé que 200 ans plus tard !\n' +
      'Le petit radis rouge, tout rond, n’a fait son apparition qu’au XVIIIe siècle.',
      '../../../assets/vegetables/radish.png'
    ),

    new Vegetable(
      'Poivron',
      'Solanaceae',
      'Légume-fruit comme la tomate, il s’intègre facilement dans toutes les recettes gorgées de soleil… Peu calorique, il est également apprécié pour ses vitamines.',
      [6, 9],
      '8 jours dans le bac à légumes',
      '35 kcal pour 100g',
      'Riche en vitamine C',
      ['Basilic', 'Bourrache', 'Carotte', 'Marjolaine', 'Origan'],
      ['Ail', 'Asperge', 'Aubergine', 'Ciboulette', 'Echalote', 'Epinard', 'Fève', 'Haricot', 'Oignon', 'Piment', 'Poireau', 'Pois', 'Pomme de terre', 'Tomate'],
      'Certainement originaire d’Amérique du Sud, le poivron a vraisemblablement été cultivé pour la première fois au Mexique. Des graines vieilles de 5 000 ans y ont été retrouvées lors de fouilles archéologiques.\n' +
      '    Le piment (à l’origine du poivron) s’est d’abord répandu en Europe au XVe siècle. Ce n’est qu’au XVIIIe siècle que sa version douce est devenue populaire dans nos contrées.\n' +
      '\n',
      '../../../assets/vegetables/sweet-peppers.png'
    ),

    new Vegetable(
      'Fenouil',
      'Apiaceae',
      'Référence symbolique et mythologique depuis l’Antiquité, c’est avant tout un aliment particulièrement intéressant sur le plan nutritionnel. On le retrouve avec plaisir dans des plats salés, mais aussi dans les desserts sucrés.',
      [9, 6],
      'Une semaine dans le bac à légumes',
      '21 kcal pour 100g',
      'Riche en vitamines K1 et B9',
      ['Aucun'],
      ['Il doit être éloigné de tous les autres légumes et aromatiques car, en règle générale, il les empêche de bien se développer.'],
      'Dès l’Antiquité, le fenouil était utilisé comme condiment.\n' +
      '    Le fenouil doux commence à être cultivé en Toscane vers la fin du Moyen Âge. Popularisé par Catherine de Médicis, il est devenu l’un des légumes préférés des Italiens.\n' +
      '    Claude Mollet, jardinier d’Henri IV et de Louis XIII l’acclimate aux potagers royaux.\n' +
      '    À la fin du XVIIe siècle, le fenouil est cultivé dans le nord de la France et aux Pays-Bas. Il reste néanmoins beaucoup plus populaire dans le sud de l’Europe.\n' +
      '    On le trouve encore aujourd’hui à l’état sauvage sur les côtes méditerranéennes.',
      '../../../assets/vegetables/fennel.png'
    ),

    new Vegetable(
      'Haricot vert',
      'Fabaceae',
      'Présenté en fagot, en salade ou en purée, il est bien plus qu’un accompagnement passe-partout. Colorées, croquantes ou fondantes, les différentes variétés allient plaisir et arguments nutritionnels.',
      [7, 9],
      '3 à 4 jours dans le bac à légumes',
      '28 kcal pour 100g',
      'Riche en fibres et vitamine B9',
      ['Bourrache', 'Brocoli', 'Capucine',  'Carotte', 'Céleri', 'Chou', 'Chou de Bruxelles', 'Chou-fleur', 'Concombre', 'Cornichon', 'Courge', 'Courgette', 'Epinard', 'Fraise', 'Laitue', 'Mâche', 'Maïs', 'Melon', 'Navet', 'Oeillet d’Inde', 'Panais', 'Radis', 'Romarin', 'Rutabaga', 'Sarriette', 'Souci'],
      ['Ail', 'Asperge', 'Aubergine',  'Betterave', 'Ciboule', 'Ciboulette', 'Echalote', 'Fenouil', 'Oignon', 'Persil', 'Piment', 'Poireau', 'Pois', 'Poivron', 'Pomme de terre', 'Tomate'],
      'Dès son arrivée en Europe, le grain sec du haricot vert est devenu l’aliment indispensable des grands voyages. Ce n’est qu’à la fin du XVIIIe siècle, en Italie du sud, qu’il a été consommé à l’état de gousse tendre.\n' +
      '    Il s’est aujourd’hui très bien adapté à la région des Grands Lacs en Afrique (Kenya, Tanzanie, Ouganda) où il retrouve un écosystème proche de celui des Andes.',
      '../../../assets/vegetables/green-beans.png'
    ),

    new Vegetable(
      'Concombre',
      'Cucurbitaceae',
      'Chaque année, il remporte un vif succès. En France, nous en consommons environ 1,8 kilos par an et par personne. Cru ou cuit, apprenez à varier les plaisirs !',
      [3, 10],
      '7 jours max. dans le bac à légumes',
      '14 kcal pour 100g',
      'Riche en potassium ',
      ['Basilic', 'Chou', 'Carottes', 'Chou-fleur', 'Haricots', 'Laitue', 'Maïs', 'Pois', 'Radis'],
      ['Pomme de terre', 'Sauge','Cornichon', 'Courge', 'Courgette', 'Melon', 'Menthe', 'Radis', 'Romarin','Tomate'],
      'Né selon toute vraisemblance dans le nord de l’Inde (où il fait l’objet de nombreux récits légendaires), le concombre s’est très tôt propagé vers la Chine et le Moyen-Orient. Il est ensuite cultivé sur les bords du Nil par les Égyptiens, qui en consomment beaucoup et le remettent en offrandes à leurs dieux. Les Hébreux l’importent également en Terre promise, où il devient l’un de leurs mets préférés.\n' +
      '\n' +
      'Durant l’Antiquité, Grecs et Romains apprécient aussi beaucoup le concombre, malgré sa forte amertume. Pline rapporte même que l’empereur Tibère s’en régalait quotidiennement, et que les jardiniers le faisaient pousser sous cloche pour accélérer sa croissance.\n' +
      'En France, on trouve mention officielle de sa présence dès le 9è siècle, lorsque Charlemagne en ordonne la culture dans ses domaines. Au 17è siècle, Louis XIV en est également très friand. Pour le lui servir en « primeur », La Quintinie, le jardinier en chef de Versailles, développera alors la production sous serre.\n' +
      '\n' +
      'Aujourd’hui, le concombre est principalement cultivé de cette manière.',
      '../../../assets/vegetables/cucumber.png'
    ),

    new Vegetable(
      'Fraise',
      'Rosacées',
      'Elles redonnent des couleurs à nos assiettes, émoustillent nos papilles… et surtout nous apportent une bonne dose de vitamines et d’antioxydants. Peu caloriques, elles peuvent même se consommer sans modération. Il ne vous reste plus qu’à trouver la variété la plus à votre goût…',
      [4, 6],
      '48h max. au frais',
      '38 kcal pour 100g',
      'Riche en fibres et vitamine C',
      ['Ail', 'Bourrache', 'Ciboulette', 'Cresson', 'Echalote', 'Epinard', 'Haricot', 'Laitue', 'Mâche', 'Navet', 'Oignon', 'Poireau', 'Sauge', 'Souci', 'Thym', 'Tomate'],
      ['Brocoli', 'Chou', 'Chou de Bruxelles', 'Chou-fleur'],
      'Dès la plus haute antiquité, elle pousse à l’état sauvage en Amérique, en Asie ainsi qu’en Europe occidentale. Les Romains en font des masques de beauté.\n' +
      '\n' +
      'En France, il faut attendre le Moyen-Âge avant que la fraise des bois n’investisse vraiment les jardins et les potagers. Plus tard, à la Renaissance, les femmes commenceront à la déguster avec de la crème, et les hommes avec du vin.\n' +
      '\n' +
      'Mais les grosses fraises que nous connaissons aujourd’hui ne feront leur apparition en France qu’en 1713, grâce à un officier de marine du nom de Frézier. Celles-ci sont issues d’un croisement entre des fraises chiliennes et des fraises d’Amérique du Nord.',
      '../../../assets/vegetables/strawberry.png'
    ),

    new Vegetable(
      'Persil',
      'Apiaceae',
      'À la fois condiment et garniture, il apporte sa fraîcheur et ses parfums à de très nombreuses préparations.',
      [4, 11],
      null,
      '38 kcal pour 100g',
      'Riche en fibres et vitamine K1',
      ['Asperge','Ciboulette', 'Maïs', 'Oignon', 'Tomates'],
      ['Laitue'],
      'Loin de nier ses nombreuses qualités pour le bien-être, l’allopathie contemporaine apporte un jugement plus modéré sur ses propriétés. Enfin, au quotidien, le plaisir d’en manger ne peut vous faire que du bien !',
      '../../../assets/vegetables/parsley.png'
    ),

    new Vegetable(
      'Melon',
      'Cucurbitaceae',
      'Rond et savoureux, il fait partie des fruits et légumes d’été préférés des Français. Il se consomme aujourd’hui surtout comme un fruit, mais agrémente également des préparations salées.',
      [6, 9],
      '6 jours max. dans le bac à légumes',
      '62 kcal pour 100g',
      'Riche en fibres et vitamine B9',
      ['Epinard', 'Fève', 'Haricot', 'Laitue', 'Maïs', 'Marjolaine', 'Pois', 'Radis',  'Tournesol'],
      ['Concombre', 'Cornichon', 'Courge', 'Courgette', 'Pomme de terre', 'Romarin', 'Sauge', 'Thym'],
      'Le melon serait originaire d’Afrique. Les Egyptiens le cultivent déjà 500 ans avant notre ère. Il gagne la Grèce puis Rome vers le 1er siècle, où il est alors consommé comme légume. Il faut dire qu’à l’époque il est petit, peu sucré ; et on le dégustait poivré et vinaigré. Sa saveur et son parfum se sont affinés petit à petit.\n' +
      '\n' +
      'Les moines le cultivent pour les papes dans leur résidence d’été de Cantaluppo, à l’époque de la Renaissance. C’est de là que vient l’appellation « Cantaloup ».\n' +
      '\n' +
      'A la fin du XVIe siècle, sa culture est largement répandue dans le midi de la France. Il se décline en plusieurs variétés aux noms évocateurs : morin, barbarin, citrolin, muscadin, etc.\n' +
      '\n' +
      'Un siècle plus tard, l’Anjou et la Touraine en produisent pour la Cour. La Charente leur emboîte alors le pas pour devenir l’une des plus importantes régions productrices et donner son nom à la variété la plus consommée aujourd’hui.',
      '../../../assets/vegetables/melon.png'
    ),

    new Vegetable(
      'Piment',
      'Solanacées',
      'Le terme piment (vert, jaune, orange, rouge, brun, pêche ou violet) est un nom vernaculaire désignant le fruit de cinq espèces de plantes du genre Capsicum de la famille des Solanacées et de plusieurs autres taxons.',
      [6, 9],
      '6 jours max. dans le bac à légumes',
      '62 kcal pour 100g',
      'Riche en fibres et vitamine B9',
      ['Chou', 'Maïs','Basilic','Bourrache','Coriandre','Carotte','Haricots','Marjolaine','Oignon','Origan','Persil', 'Tomates'],
      ['Ail', 'Asperge', 'Aubergine', 'Ciboulette', 'Echalote', 'Epinard', 'Fève', 'Haricot', 'Oignon', 'Poireau', 'Pois', 'Poivron', 'Pomme de terre', 'Tomate'],
      'Les piments font partie de l\'alimentation des Amériques depuis au moins 9 500 ans4. Des traces archéologiques de domestication ont été trouvées dans le Sud-Ouest de l\'Équateur il y a plus de 6 000 ans. Le piment semble avoir été une des premières plantes cultivées autogames en Amérique centrale et du Sud4,8. ',
      '../../../assets/vegetables/chilli-pepper.png'
    ),

    new Vegetable(
      'Pois',
      'Fabaceae',
      'Largement importé, il est apprécié au printemps. Sa saveur printanière est un régal dans des préparations simples et traditionnelles. On l’appelle aussi pois mange-tout ou princesse. Légume plaisir, il est l’allié de votre bien-être grâce à ses qualités nutritionnelles atypiques.',
      [1, 12],
      'Le jour même',
      '30 kcal pour 100g',
      'Riche en fibres et vitamine C',
      ['Artichaut', 'Brocoli', 'Carotte', 'Céleri-branche','Céleri-rave','Chou', 'Chou de Bruxelles', 'Chou-fleur', 'Concombre', 'Cornichon', 'Courge', 'Courgette', 'Epinard', 'Laitue', 'Maïs', 'Melon', 'Menthe', 'Navet', 'Panais', 'Radis', 'Rutabaga'],
      ['Ail', 'Asperge', 'Aubergine', 'Ciboule',  'Ciboulette', 'Echalote', 'Fenouil', 'Fève', 'Fraise', 'Haricot', 'Oignon', 'Persil', 'Piment', 'Poireau', 'Poivron', 'Pomme de terre', 'Tomate'],
      'Le pois mange-tout était probablement déjà cultivé en Europe au VIIIe siècle. Il était encore vraisemblablement consommé sec.\n' +
      '    Au début du XVIe siècle, les Hollandais auraient été les premiers à consommer la gousse fraîche entière avec ses graines.\n' +
      '    Monsieur de Buhy, ambassadeur de France aux Pays-Bas aurait ainsi ramené une cosse de pois gourmand à la cour de France au XVIIe siècle.\n' +
      '    Très populaire dans la cuisine rurale, le pois gourmand a été fortement concurrencé par sa variété sœur, le petit pois à écosser.\n' +
      '\n',
      '../../../assets/vegetables/pea.png'
    ),

    new Vegetable(
      'Rhubarbe',
      'Polygonaceae',
      'Elle est aujourd’hui très bien acclimatée en France et se trouve sur les étals au printemps, mais aussi en hiver ; il s’agit alors de rhubarbe importée. Sa chair acidulée surprend et réveille les papilles, et se prête à des desserts gourmands, ou plus surprenant, en accompagnement de plats salés. Ce fruit pauvre en calories et riche en calcium se cuisine facilement et plaît aux enfants comme aux plus grands.',
      [4, 7],
      '2 à 3 jours dans le bac à légumes',
      'Non renseigné',
      'Non renseigné',
      ['Ail', 'Asperge', 'Cerfeuil', 'Epinard'],
      ['Pissenlit'],
      '\n' +
      '\n' +
      'Le mot rhubarbe vient du latin reuburbarum qui signifie, littéralement, « racine barbare ». Ce nom lui vient du fait qu’elle n’était consommée et appréciée que des peuples étrangers, appelés alors « barbares ».\n' +
      '\n' +
      'Marco Polo la ramène d’un de ses voyages en Asie au XVIe siècle. On ne lui prête alors que des vertus curatives, notamment la faculté de soigner les maladies vénériennes.\n' +
      '\n' +
      'Ce sont finalement les Anglais qui introduisent la plante en tant qu’aliment en Europe. La France commence à la cultiver au XIXe siècle. Cette herbe vivace s’est finalement très bien acclimatée dans nos contrées, et régale aujourd’hui nombre de consommateurs français. On la trouve principalement dans le nord et l’est de la France, mais elle pousse également dans les jardins, pour peu qu’elle reçoive suffisamment d’eau.\n',
      '../../../assets/vegetables/rhubarb.png'
    ),

    new Vegetable(
      'Romarin',
      'Lamiaceae',
      'Le Romarin, Salvia rosmarinus, est une espèce d’arbrisseaux de la famille des Lamiacées (ou Labiées), poussant à l’état sauvage sur le pourtour méditerranéen, en particulier dans les garrigues arides et rocailleuses, sur terrains calcaires.',
      [1, 12],
      'Non renseigné',
      'Non renseigné',
      'Non renseigné',
      ['Brocoli', 'Carotte', 'Chou pommé', 'Chou de Bruxelles', 'Chou-fleur', 'Haricot', 'Navet', 'Persil', 'Sauge'],
      ['Concombre', 'Cornichon', 'Maïs'],
      'Cette section n\'est pas encore complétée',
      '../../../assets/vegetables/rosemary.png'
    ),

    new Vegetable(
      'Pomme',
      'Rosaceae ',
      'Ses techniques de production se sont perfectionnées au cours des siècles, offrant un large éventail de variétés et de saveurs. La pomme rythme tous les moments de la journée, croquée au naturel ou cuisinée pour des mets sucrés ou salés.',
      [10, 4],
      '7 à 8 jours à l’air ambiant',
      '54 kcal pour 100g',
      'Riche en fibres',
      ['Non renseigné'],
      ['Non renseigné'],
      'Apparue sur Terre il y a quelque quatre-vingt millions d’années, la pomme poussait à l’état sauvage dans le sud du Caucase jusqu’au Sinkiang (Ouest de la Chine). D’abord consommée dans cette région et sur les plateaux d’Anatolie (Turquie actuelle) par l’homme du Néolithique, elle gagne l’Europe à la faveur des périodes de réchauffements climatiques. Des vestiges de pommes ont ainsi été retrouvés dans les villages lacustres néolithiques de Suisse et d’Italie du Nord.\n' +
      '\n' +
      'Personnage de légende\n' +
      '\n' +
      'Trois siècles avant notre ère déjà, Théophraste distingue six variétés de pommes dans son Histoire des Plantes. Le fruit ne se développe pas qu’autour de la Méditerranée, mais aussi dans les imaginations : les récits mythologiques lui réservent le haut du panier.\n' +
      'Offerte par Pâris à Aphrodite, la pomme est à l’origine de la guerre de Troie. Et pour son avant-dernier travail, Hercule vole celles du jardin des Hespérides.\n' +
      '\n' +
      'Les siècles passent, les variétés se multiplient\n' +
      '\n' +
      'Les Romains participent à l’implantation du fruit dans une grande partie de l’Europe, au gré de leurs conquêtes, et se régalent d’une trentaine de variétés. Dix siècles plus tard, une vingtaine de plus sont répertoriées par le célèbre agronome Olivier de Serres.\n' +
      'Elles évoluent au fil des siècles, sélectionnées pour leurs qualités gustatives, leur résistance, leur rendement,etc. Pour autant, les variétés plus fragiles ne disparaissent pas et demeurent cultivées et consommées localement. La pomme est le fruit du terroir par excellence.\n',
      '../../../assets/vegetables/apples.png'
    ),

    new Vegetable(
      'Brocoli',
      'Brassicaceae',
      'Ce grand parent du chou-fleur est présent depuis quelques dizaines d’années sur les étals français, et a très vite su se faire une place dans les assiettes. Son goût très doux et son croquant font des merveilles dans tous types de recettes salées. Gorgé de vitamines, ce légume vert participe en plus à votre bien-être !',
      [6, 11],
      'Entre 4 et 5 jours dans le bac à légumes.',
      '37 kcal pour 100g',
      'Riche en vitamine B9 et K1 ',
      ['Carottes', 'Concombre', 'Haricots','Aneth', 'Betterave', 'Camomille', 'Capucine',  'Céleri', 'Concombre', 'Cornichon', 'Epinard',  'Fève', 'Haricot', 'Nain', 'Menthe', 'Origan', 'Panais', 'Pois', 'Pomme de terre', 'Romarin', 'Thym'],
      ['Ail', 'chou', 'Chou de Bruxelles', 'Chou-fleur', 'Echalote', 'Fraise', 'Laitue', 'Navet', 'Oignon', 'Poireau', 'Radis', 'Rutabaga', 'Sauge', 'Tomate'],
      '\n' +
      '\n' +
      'Originaire d’Italie, le brocoli n’est autre qu’un lointain descendant du chou sauvage et du chou-fleur . Et comme ses ancêtres, on n’en consomme que la pomme.\n' +
      '\n' +
      'Création des Romains, le brocoli a été développé à partir des plus beaux spécimens de chou sauvage ;  il s’agit même de l’étape intermédiaire avant la découverte du chou-fleur.\n' +
      '\n' +
      'Très vite apprécié des Italiens, le légume vert a été popularisé par Catherine de Médicis, qui s’en régale sous la Renaissance. Cette dernière a en effet coutume de la surnommer l’« asperge italienne ».\n' +
      '\n' +
      'Les Anglais adoptent le brocoli au XVIIIe siècle, tandis que les Américains n’en consomment qu’au terme du XIXe siècle.\n' +
      '\n' +
      'Mais si le brocoli est cultivé depuis le XVIIe siècle en Europe, il n’a été introduit que très tardivement en France. Le légume n’a commencé à être exploité en Bretagne que dans les années 1980.\n' +
      '\n' +
      'Le saviez-vous ?\n' +
      '\n' +
      'Brocoli vient de l’italien brocco, qui signifie « pousse ». Broccolo (broccoli au pluriel) est un diminutif du terme original.\n',
      '../../../assets/vegetables/broccoli.png'
    ),

    new Vegetable(
      'Chou-fleur',
      'Brassicaceae',
      'Cru ou cuit, il se prête à de multiples recettes et présente de sérieuses qualités nutritionnelles. Vous pouvez donc en manger sans compter !',
      [9, 4],
      '2 à 3 jours dans le bac à légumes',
      '30 kcal pour 100g',
      'Riche en vitamine C et B9',
      ['Aneth', 'Artichaut', 'Blette', 'Betterave', 'Bourrache', 'Camomille', 'Capucine',  'Carotte', 'Céleri', 'Concombre', 'Cornichon', 'Epinard', 'Fève', 'Haricot nain', 'Hysope', 'Laitue', 'mâche', 'Menthe', 'œillet d’Inde', 'Origan', 'Pois', 'Pomme de terre',  'Romarin', 'Sarriette', 'Sauge', 'Souci', 'Thym'],
      ['Ail', 'Brocoli','Ciboulette', 'Chou', 'Chou-fleur', 'Echalote', 'Fraise', 'Navet', 'Oignon', 'Poireau', 'Radis', 'Rutabaga', 'Tomate', 'Raisin'],
      '\n' +
      '\n' +
      'Le chou-fleur est probablement originaire du Proche-Orient. Les Grecs et les Romains le connaissent et l’apprécient dès l’Antiquité. Mais celui-ci tombe ensuite dans l’oubli pendant une longue période.\n' +
      '\n' +
      'En Europe, on ne redécouvre le chou-fleur qu’au XVIe siècle, grâce à des marins italiens qui le ramènent du Levant… Mais en France, il devient surtout tendance au XVIIe siècle, grâce au jardinier de Louis XIV, La Quintinie. Celui-ci importe alors ses graines de Chypre.\n' +
      '\n' +
      'Quelques années plus tard, on parvient à développer sa culture par semis. Le chou-fleur, alors très apprécié du roi Louis XV, devient populaire.\n' +
      '\n' +
      'Dès la fin du XIXe siècle, près de vingt variétés de choux-fleurs sont produites dans l’Hexagone, devenu l’un des pays spécialistes de ce légume.\n',
      '../../../assets/vegetables/cauliflower.png'
    ),

    new Vegetable(
      'Céleri-branche',
      'Apiaceae',
      ' Sa saveur marquée et sa texture croquante le rendent tout aussi délicieux cru que cuit. Par petites touches ou en ingrédient vedette, le céleri-branche se prépare et s’accommode avec facilité, tout en vous permettant de faire le plein de vitalité. Alors, prêt ? Croquez !',
      [7, 1],
      '5 jours dans le bac à légumes',
      '14 kcal pour 100g',
      null,
      ['Chou', 'Chou-fleur', 'Pois', 'Tomates'],
      ['Pomme de terre'],
      '\n' +
      '\n' +
      'Provenant de la péninsule italienne, les Grecs en faisaient un usage quotidien dès l’Antiquité comme plante médicinale puis aromatique.\n' +
      '\n' +
      'Le céleri-branche n’est cuisiné qu’à partir de la Renaissance, mais ce n’est encore qu’un condiment. Après un détour par l’Allemagne, le légume se propage dans les assiettes françaises au cours du XIXe siècle.\n',
      '../../../assets/vegetables/celery.png'
    ),

    new Vegetable(
      'Poireau',
      'Alliaceae',
      'Légume résistant, il entre dans la composition de nombreux plats grâce à son parfum si particulier et à sa saveur aillée. Originaire du Moyen-Orient, il a traversé les siècles pour être aujourd’hui le neuvième légume le plus consommé par les Français.',
      [9, 4],
      '5 jours dans le bac à légumes',
      '27 kcal pour 100g',
      'Riche en fibres et vitamine B9',
      ['Carotte', 'Céleri-branche','Céleri-rave','Endive', 'Epinard', 'Fraise', 'Laitue', 'Mâche', 'Oeillet d’Inde', 'Panais', 'Tomate'],
      ['Ail', 'Asperge', 'Blette', 'Betterave', 'Brocoli', 'Chou', 'Chou de Bruxelles', 'Chou-fleur', 'Fève', 'Haricot', 'Oignon', 'Persil', 'Piment', 'Pois', 'Poivron', 'Sauge'],
      '\n' +
      '    Le poireau proviendrait du Moyen-Orient mais ses origines précises restent très floues. Il était déjà cultivé dans l’Égypte ancienne où il symbolisait la victoire : le pharaon Kheops en offrait à ses meilleurs guerriers.\n' +
      '    Les Romains l’appréciaient beaucoup. Le tristement célèbre empereur Néron en consommait en grande quantité pour adoucir ses cordes vocales. Cela lui valut le nom de « porrophage ».\n' +
      '    Il fut introduit en Grande-Bretagne par les Romains. Lors d’une guerre, les soldats gallois en avaient planté dans leurs chapeaux comme signe de reconnaissance. Cela leur valut la victoire. C’est ainsi qu’il devint le symbole du pays de Galles.\n' +
      '    Au Moyen Âge, il était souvent utilisé dans les soupes appelées « porées », d’où son nom actuel.\n',
      '../../../assets/vegetables/leek.png'
    ),

    new Vegetable(
      'Chou rouge',
      'Brassicaceæ',
      'Avec sa tête bien ferme et ses feuilles lisses, le chou rouge fait partie des choux cabus. De forme ronde ou ovale, il pèse en moyenne 1,5 kg. Généralement pourpre, sa couleur varie du rouge au violet selon l’acidité du sol dans lequel il a poussé.',
      [10, 2],
      '1 semaine au frais',
      '30 kcal pour 100g',
      'Source de vitamines C et B9',
      ['Aneth', 'Artichaut', 'Blette', 'Betterave', 'Bourrache', 'Camomille', 'Capucine',  'Carotte', 'Céleri', 'Concombre', 'Cornichon', 'Epinard', 'Fève', 'Haricot nain', 'Hysope', 'Laitue', 'mâche', 'Menthe', 'œillet d’Inde', 'Origan', 'Pois', 'Pomme de terre',  'Romarin', 'Sarriette', 'Sauge', 'Souci', 'Thym'],
      ['Ail', 'Brocoli','Ciboulette', 'Chou', 'Chou-fleur', 'Echalote', 'Fraise', 'Navet', 'Oignon', 'Poireau', 'Radis', 'Rutabaga', 'Tomate', 'Raisin'],
      'Originaire d’Europe centrale, le chou rouge conquit le bassin méditérannéen pendant l’Antiquité. En France, c’est Catherine de Médicis qui le rapporta d’Italie au XVIe siècle et encouragea sa culture. Pendant longtemps le chou rouge fut apprécié pour ses vertus médicinales : il était utilisé notamment pour lutter contre les affections pulmonaires et les bronchites ou pour traiter les inflammations du tube digestif et la cirrhose du foie. Les Grecs, quant à eux, le servaient en soupe aux jeunes mariés au lendemain de leurs noces afin de favoriser la fertilité ou le croquaient cru pour empêcher les effets néfastes de l’abus d’alcool… Le saviez-vous ? Généralement pourpre, la couleur des feuilles du chou rouge varie du rouge fuchsia au bleuté selon l’acidité du sol dans lequel il a poussé. En effet, le chou rouge contient des colorants (les anthocyanes) qui ont la propriété de changer de couleur en fonction du pH, ce qui en fait un très bon indicateur de pH naturel, utilisé pour enseigner les réactions acido-basiques en cours de chimie !',
      '../../../assets/vegetables/red-cabbage.png'
    ),

    new Vegetable(
      'Chou kale',
      'Brassicaceæ',
      'C’est d’ailleurs le seul chou à se présenter sous la forme de feuilles… sans tête ! Encore peu connu en France, le kale possède beaucoup d’autres noms. Parmi lesquels, le « chou frisé » ou le « chard ».',
      [10, 3],
      '1 à 2 jours dans le bac à légumes',
      '27 kcal pour 100 g',
      'Source de vitamines C, B5, B6',
      ['Aneth', 'Artichaut', 'Blette', 'Betterave', 'Bourrache', 'Camomille', 'Capucine',  'Carotte', 'Céleri', 'Concombre', 'Cornichon', 'Epinard', 'Fève', 'Haricot nain', 'Hysope', 'Laitue', 'mâche', 'Menthe', 'œillet d’Inde', 'Origan', 'Pois', 'Pomme de terre',  'Romarin', 'Sarriette', 'Sauge', 'Souci', 'Thym'],
      ['Ail', 'Brocoli','Ciboulette', 'Chou', 'Chou-fleur', 'Echalote', 'Fraise', 'Navet', 'Oignon', 'Poireau', 'Radis', 'Rutabaga', 'Tomate', 'Raisin'],
      'Au Moyen Âge, il était déjà un aliment de base chez les paysans : il pousse en effet facilement et résiste à des températures très froides : jusqu’à – 15°C ! C’est surtout dans les pays d’Europe du Nord (Ecosse, Allemagne, Hollande, Scandinavie) que sa culture s’est implantée durablement. Il faudra ensuite attendre le XVIIème siècle pour que les Anglais l’introduisent aux Etats-Unis.',
      '../../../assets/vegetables/kohlrabi.png'
    ),

    new Vegetable(
      'Chou de Bruxelles',
      'Brassicaceæ',
      'Notamment grâce à des saveurs moins amères et plus sucrées, davantage adaptées aux goûts actuels. Les jeunes chefs l’ont bien compris : le chou de Bruxelles n’est plus exclusivement le symbole d’une nourriture rustique traditionnelle mais peut jouer la carte de la modernité en s’accomodant de mille façons.',
      [9, 3],
      '7 jours dans le bac à légumes',
      '45 kcal pour 100g',
      'Riche en vitamine K1 et C',
      ['Aneth', 'Artichaut', 'Blette', 'Betterave', 'Bourrache', 'Camomille', 'Capucine',  'Carotte', 'Céleri', 'Concombre', 'Cornichon', 'Epinard', 'Fève', 'Haricot nain', 'Hysope', 'Laitue', 'mâche', 'Menthe', 'œillet d’Inde', 'Origan', 'Pois', 'Pomme de terre',  'Romarin', 'Sarriette', 'Sauge', 'Souci', 'Thym'],
      ['Ail', 'Brocoli','Ciboulette', 'Chou', 'Chou-fleur', 'Echalote', 'Fraise', 'Navet', 'Oignon', 'Poireau', 'Radis', 'Rutabaga', 'Tomate', 'Raisin'],
      'Au XVIIème siècle, afin d’optimiser leur rendement, les maraîchers de Saint-Gilles créèrent une nouvelle variété de chou qui se cultivait à la verticale et prenait ainsi moins d’espace. Très rentable, cette culture se répandit rapidement sur de grands espaces. Elle permit de nourrir la population en forte croissance de Bruxelles et valut aux Saint-Gillois le surnom de « Kuulkappers » (coupeurs de choux).',
      '../../../assets/vegetables/brussel-sprouts.png'
    ),

    new Vegetable(
      'Chou pommé',
      'Brassicaceæ',
      'À chaque région de France sa spécialité à base de chou. Longtemps associé à des plats roboratifs, le chou retrouve aujourd’hui toute sa place dans des préparations légères et raffinées.',
      [10, 4],
      '7 jours dans le bac à légumes',
      '25 kcal pour 100g',
      'Riche en vitamine B9 et C',
      ['Aneth', 'Artichaut', 'Blette', 'Betterave', 'Bourrache', 'Camomille', 'Capucine',  'Carotte', 'Céleri', 'Concombre', 'Cornichon', 'Epinard', 'Fève', 'Haricot nain', 'Hysope', 'Laitue', 'mâche', 'Menthe', 'œillet d’Inde', 'Origan', 'Pois', 'Pomme de terre',  'Romarin', 'Sarriette', 'Sauge', 'Souci', 'Thym'],
      ['Ail', 'Brocoli','Ciboulette', 'Chou', 'Chou-fleur', 'Echalote', 'Fraise', 'Navet', 'Oignon', 'Poireau', 'Radis', 'Rutabaga', 'Tomate', 'Raisin'],
      'Les premières traces du chou pommé dans l’Histoire remontent à l’Antiquité. Théophraste le mentionne dans ses écrits 300 ans avant notre ère. Son importance majeure dans l’alimentation des Français remonte au Moyen Âge, période à laquelle sa culture se développe. Facile à cultiver, le chou pommé se développe sous des climats variés, ce qui explique le grand nombre de ses variétés. Au début du XXe siècle, on dénombre ainsi quelque 250 espèces différentes en France.',
      '../../../assets/vegetables/cabbage.png'
      ),

    new Vegetable(
      'Artichaut',
      'Asteraceae',
      'Cette fleur comestible à la saveur fine garnit les tables au printemps et à l’été. Délicate et généreuse, la chair de l’artichaut est gorgée d’antioxydants.',
      [3, 9],
      'Quelques jours dans le bac à légumes',
      '47 kcal pour 100',
      'Riche en vitamine B9',
      ['Asperge', 'Aubergine', 'Chou', 'Chou de Bruxelles', 'Chou-fleur', 'Fève', 'Haricot', 'Oignon', 'Pois', 'Radis','Tomate'],
      ['Endive', 'Estragon', 'Laitue', 'Persil', 'Salsifis', 'Topinambour', 'Tournesol'],
      'Originaire du bassin méditerranéen, l’artichaut est le résultat de différents croisements botaniques. Les premières traces de ce légume sont relevées en Italie, en pleine Renaissance (milieu du XVIe siècle). L’apanage des rois: L’artichaut est introduit pour la première fois en France sur la table de la reine Catherine de Médicis. Le légume devient également le péché mignon de Louis XIV, qui l’appréciait tellement qu’on dénombre 5 espèces différentes à Versailles à l’époque du Roi Soleil : le Blanc, le Vert, le Violet, le Rouge et le Sucré de Gênes. Ce n’est qu’en 1810 qu’un agronome de la région parisienne développe le Camus de Bretagne, l’artichaut favori des Français. Le saviez-vous ? Ce que l’on appelle communément le cœur ou le fond d’artichaut n’est autre que le réceptacle des fleurs non épanouies de la plante. Ces dernières forment alors le foin, sorte de poils qui recouvre le cœur d’artichaut.',
      '../../../assets/vegetables/artichoke.png'
    ),

    new Vegetable(
      'Blette',
      'Chenopodiaceæ',
      'Si sa culture s’est réduite depuis le début du XXe siècle, elle n’en reste pas moins appréciée, pour sa saveur douce et sa texture fondante et on la trouve sur la plupart des marchés. Ce légume très méridional est un délicieux accompagnement qui peut être cuisiné de multiples façons, sans compter qu’il concentre quantité de vertus nutritionnelles.',
      [6, 11],
      '1 à 2 mois dans le bac à légumes',
      '16 kcal pour 100g',
      'Riche en vitamine K1',
      ['Ail', 'Camomille', 'Céleri', 'Chou', 'Chou de Bruxelles', 'Chou-fleur', 'Echalote', 'Laitue', 'Mâche', 'Navet', 'Oignon', 'Radis'],
      ['Asperge', 'Betterave', 'Carotte', 'Epinard', 'Panais', 'Poireau', 'Pomme de terre', 'Tomate'],
      'D’après les botanistes et les historiens, la blette aurait vu le jour à la chaleur du bassin méditerranéen. Des traces de culture et de consommation ont été trouvées tant en Mésopotamie que dans la Rome Antique. Au Moyen-Âge, elle se consomme en soupe, la « porée » (ou « poirée »), qu’on additionne de poireau. Ce légume rustique également appelé bette ou carde est très apprécié des consommateurs, si bien qu’il est cultivé en grandes quantités. Les espèces sont sélectionnées pour fournir des côtes bien larges. Au XVIe siècle, le botaniste Bauhin en répertorie des variétés de différentes couleurs : blanches, vertes, mais aussi jaune et rouge. Si la fin du XIXe siècle voit la culture d’une dizaine de variétés, le début du siècle suivant voit sa consommation décliner peu à peu avant de connaître un regain ces dernières décennies.',
      '../../../assets/vegetables/chard.png'
    ),

    new Vegetable(
      'Fève',
      'Fabaceae',
      'Elle se croque crue ou bien cuite. Grâce à sa facilité de conservation sèche, elle fut pendant longtemps un aliment de réserve essentiel face aux pénuries alimentaires.',
      [5, 6],
      '2 jours max. dans le bac à légume',
      '82 kcal pour 100 g',
      'Riche en vitamine B9',
      ['Artichaut', 'Bourrache', 'Brocoli', 'Carotte', 'Céleri', 'Chou',' Chou de Bruxelles', 'Chou-fleur', 'Concombre', 'Cornichon', 'Courge', 'Courgette', 'Epinard', 'Fraisier', 'Laitue', 'Maïs', 'Melon', 'Navet', 'Oeillet d’Inde', 'Radis', 'Rutabaga', 'Tomate'],
      ['Ail', 'Asperge', 'Aubergine', 'Betterave', 'Ciboulette', 'Echalote', 'Haricot', 'Oignon', 'Piment', 'Poireau', 'Poivron', 'Pomme de terre', 'Pois'],
      'On suppose que la fève est originaire du Moyen-Orient, plus précisément du sud de la mer Caspienne. Elle s’est ensuite rapide installée en Europe, en Afrique et en Inde. Très populaire durant l’Antiquité et au Moyen Âge, elle cède progressivement mais sûrement sa place dans les assiettes aux haricots et à la pomme de terre. Ce n’est qu’à partir du XVe et du XVIe siècle que la fève est consommée fraîche. On la cultivait alors le long des rivières, ce qui lui a valu son nom de « fève des marais ».',
      '../../../assets/vegetables/jelly-beans.png'
    ),

    new Vegetable(
      'Champignon de Paris',
      'Agaricaceæ',
      'Tendre et goûteux, ce champignon de couche se cuisine facilement.',
      [9, 6],
      '3 ou 4 jours au réfrigérateur',
      '38 kcal pour 100 g',
      'Riche en vitamine B2, B3, B5, B9 et D',
      ['Non Renseigné'],
      ['Non Renseigné'],
      'L’homme, friand du champignon, apprend rapidement à le cultiver. Les Grecs et les Romains obtiennent ainsi la pholiote en recouvrant les racines de figuier d’un mélange de fumier et de cendre. Reprise à la Renaissance, cette technique se diffuse. Du potager de Versailles aux carrières abandonnées: La Quintinie, jardinier de Louis XIV à Versailles, fait beaucoup évoluer la culture des champignons. À cette époque, la production était limitée. Le froid de l’hiver tuait les plants, tandis que les parasites et la chaleur en venaient à bout l’été. En 1810, un horticulteur a l’idée de cultiver des champignons dans les carrières abandonnées au sud de la capitale… C’est ainsi que naquirent les célèbres et savoureux champignons de Paris! Toutes les conditions sont réunies pour que le légume prospère : constance de la température, ensoleillement limité, régulation de l’afflux d’air, contrôle du taux d’hygrométrie. La culture en cave se développe pendant tout le XIXe siècle, en région parisienne et dans le Val de Loire.',
    '../../../assets/vegetables/mushrooms.png'

    ),
    new Vegetable(
      'Champignon cultivé',
      'Agaricaceæ',
      'Ils appartiennent au groupe des « fungi », et plus exactement à la catégorie des saprophytes. Ils sont à distinguer des champignons sauvages, dont la présence, sujette aux aléas de la météo, est irrégulière et le plus souvent très courte.',
      [9, 6],
      '4 jours max. dans le bac à légumes',
      '38 kcal pour 100g',
      'Riche en vitamine B5',
      ['Non Renseigné'],
      ['Non Renseigné'],
      'Ramsès II, Toutânkhamon ou encore Nefertiti ont vraisemblablement dégusté des champignons ! Des fouilles ont en effet mis à jour des fresques murales, sur lesquelles ils étaient représentés, dans un tombeau de pharaons égyptiens, datant de 1450 ans avant J. C. Les Grecs, eux, les faisaient pousser en recouvrant de fumier et de cendres des racines de figuier ensemencées de mycélium. Les Romains en raffolaient comme condiment. En France, le jardinier de Louis XIV, Jean-Baptiste de la Quintinie, cultive du « blanc de Paris » dans les jardins de Versailles, sur des meules de fumier (au printemps et en automne uniquement). Il faut attendre 1814 pour qu’un maraîcher décide de cultiver ses champignons à l’abri de caves du sous-sol parisien. La température constante permet de récolter tout au long de l’année. Au passage, le champignon en question hérite d’un nom prestigieux : le champignon de Paris ! La construction du métro parisien a favorisé le déplacement des cultures vers les galeries des carrières autour de la capitale et du Val de Loire, d’où était extrait le calcaire utilisé pour construire les châteaux. Aujourd’hui, la culture en caves et carrières est délaissée au profit de maisons de cultures : la température, l’hygrométrie et l’hygiène sont plus faciles à maîtriser, ce qui améliore les conditions de travail.',
      '../../../assets/vegetables/mushroom.png'
    ),

    new Vegetable(
      'Cèpe',
      'Boletaceæ',
      'Sa saveur est tellement exceptionnelle qu’il est très attendu et convoité, dès le début de l’automne, sur les étals. Ces petits « bouchons » gorgés de vitamines sont délicieux cuisinés simplement, ou intégrés à des recettes plus élaborées, voire à des plats de fête.',
      [8, 11],
      '2 jours max.',
      '28 kcal pour 100g',
      'Riche en vitamine du groupe B',
      ['Non Renseigné'],
      ['Non Renseigné'],
      'Le cèpe est un champignon sylvestre. Par conséquent, il pousse dans des terrains propices à son développement : bois et forêts ; mais on le trouve aussi plus rarement dans les prés ou dans les champs. Certaines conditions climatiques doivent également être réunies pour que le cèpe puisse s’épanouir. Ainsi, le cèpe se récolte 10 jours après une période chaude interrompue par une forte averse ou une chute de grêle, généralement vers la fin de l’été/début de l’automne.',
      '../../../assets/vegetables/boletus.png'
    ),

    new Vegetable(
      'Morille',
      'Morchellaceae',
      'Ce champignon sauvage embaume les plats avec un parfum unique, très subtil. Reconnue sur le plan gustatif, elle présente également de vrais arguments nutritionnels.',
      [4, 5],
      '2 jours dans un endroit sec et frais',
      '25 kcal pour 100g',
      'Riche en vitamine B, D et E',
      ['Non Renseigné'],
      ['Non Renseigné'],
      'Au Moyen Âge et à la Renaissance, c’était un champignon particulièrement recherché. Au printemps 1945, on trouvait un grand nombre de morilles sur les plages du débarquement. Elles avaient fait des cratères et trous d’obus un terrain propice à leur croissance.',
      '../../../assets/vegetables/morel.png'
    ),

    new Vegetable(
      'Truffe',
      'Tubéracées',
      'C’est le fruit de la rencontre entre un arbre, dit « arbre truffier » (chêne, hêtre, tilleul, frêne, charme, noisetier…) et un champignon ascomycète ectomycorhizien, qui vit au contact des radicelles de l’arbre. Les excroissances produites par le champignon s’appellent… les truffes !',
      [9, 3],
      '5 à 6 jours maximum',
      '284 kcal pour 100g',
      'Riche en vitamine A, B2, B3, B5, D et K',
      ['Non Renseigné'],
      ['Non Renseigné'],
      'Égyptiens, Grecs et Romains louaient son arôme et lui prêtaient parfois des vertus aphrodisiaques, voire médicinales. Au Moyen-Âge, elle est délaissée : on voit en elle une création diabolique… Elle revient ensuite peu à peu sur les tables, et Louis XIV lui redonne définitivement ses lettres de noblesse.',
      '../../../assets/vegetables/truffle.png'
    ),

    new Vegetable(
      'Cresson',
      'Brassicaceæ',
      'Sa production, très délicate, explique encore la faible superficie d’exploitation. Présente toute l’année sur les étals, cette exquise petite salade accompagne et sublime vos recettes salées. Et pour ne rien gâcher, le cresson est riche en vitamines et minéraux !',
      [9, 5],
      '2 jours max. dans le bac à légumes',
      '14 kcal pour 100g',
      'Riche en vitamines K1 et C',
      ['Fraise',' Radis noir', 'Tomate'],
      ['Tous les légumes sauf : Fraise, Radis noir et Tomate'],
      'Cueilli à l’état sauvage, il n’a été cultivé en France qu’au début du XIXe siècle, sous l’impulsion du directeur des Hôpitaux de la Grande Armée. Ce dernier avait en effet observé les cressonnières allemandes (datant du XVIIe siècle), recouvertes d’un tapis de verdure en plein cœur de l’hiver. Les premières plantations furent ainsi installées dans la région de Senlis et de Chantilly, dans l’Oise.',
      '../../../assets/vegetables/watercress.png'
    ),

    new Vegetable(
      'Kumquat',
      'Rutaceae',
      'Cet arbuste à feuillage persistant et à croissance lente peut mesurer 4 m de haut. Ses rameaux portent parfois de petites épines. Contrairement aux agrumes du genre Citrus, il est relativement rustique puisqu\'il peut supporter une température de -10°C. Ses feuilles sont vert foncé brillant, et les fleurs d\'un blanc pur, semblable aux autres fleurs d\'agrumes. Le kumquat est un petit fruit rond ou ovale de 2 à 5 cm de long. Sa peau jaune-orange est comestible. Sa chair est acidulée. Il peut être consommé frais, souvent avec la peau qui est tendre ce qui donne une saveur plus forte et légèrement amère. Il est très cultivé en Asie mais également en Afrique, en Amérique, sur l\'île grecque de Corfou et dans le sud de la France, particulièrement à Toulon. On le trouve également sur les marchés à La Réunion, où il se mange généralement tel quel.',
      [11, 3],
      '2 semaines max. au réfrégirateur',
      '55 kcal pour 100g',
      'Riche en vitamine C',
      ['Non Rensesigné'],
      ['Non Renseigné'],
      'Les kumquats ont été introduits en Europe en 1846 par Robert Fortune, collecteur pour la Royal Horticultural Society, puis peu de temps après en Amérique du Nord. Initialement placées dans le genre Citrus, ces espèces ont été transférées dans le genre Fortunella en 1915, toutefois tous les travaux plus récents (Burkill 1931, Mabberley 1998) préconisent leur retour au sein du genre Citrus.',
      '../../../assets/vegetables/kumquat.png'
    ),

    new Vegetable(
      'Groseille',
      'Grossulariaceae',
      'Prenez une grappe bien rouge, croquez-en les grains et laissez leur saveur acidulée exploser en bouche ! Idéal pour les apéritifs et les desserts estivaux, ce fruit ajoute aussi sa note vitaminée à vos petits plats. Alors prenez prétexte des excellents apports nutritionnels de la groseille pour vous faire plaisir !',
      [6, 9],
      '2 à 3 jours dans le bac à légumes',
      '68 kcal pour 100g',
      'Riche en vitamine C',
      ['Non Renseigné'],
      ['Non Renseigné'],
      'Les ancêtres sauvages du groseillier poussent dans les pays du nord de l’Europe ou dans les régions montagneuses froides. Pour ces raisons, le groseillier reste longtemps inconnu des grecs et romains antiques. Cultivée dans les jardins français depuis le Moyen-Âge, sa présence est attestée au XIIe siècle en Lorraine, et au XVIe siècle dans les jardins et à la table du roi de France. En Angleterre, on l’utilisait pour garnir et assaisonner le maquereau.La capitale de la groseille: Depuis le Moyen-Âge, la ville lorraine de Bar-le-Duc est mondialement réputée pour sa confiture de groseilles sans pépins. Pour confectionner ce mets très recherché, les baies sont délicatement épépinées à l’aide d’une plume d’oie dont la pointe est finement taillée en biseau. Ce procédé permet au fruit de garder sa consistance avant cuisson.',
      '../../../assets/vegetables/redcurrent.png'
    ),

    new Vegetable(
      'Mûre',
      'Rosaceae',
      'Fruit gourmand par excellence, la mûre est également cultivée. Plus douce et plus sucrée, la mûre de culture offre, elle aussi, tout un panel de saveurs. Le petit fruit à la belle robe sombre est une habituée de la cuisine. Elle est aussi une alliée incontournable de votre bien-être.',
      [7, 10],
      '2 à 3 jours dans le bac à légumes',
      '45 kcal pour 100g',
      'Riche en vitamine C et K1',
      ['Non Renseigné'],
      ['Non Renseigné'],
      'Présent un peu partout, cet arbrisseau vivace, rampant (et piquant !), est souvent considéré comme une plante envahissante. La mûre s’invite dans la mythologie grecque : elle serait issue du sang des Titans, versé lors de leurs luttes contre les dieux. Au XVe siècle, la mûre est consommée en hors-d’œuvre, à la croque, tout simplement.',
      '../../../assets/vegetables/blackberry.png'
    ),

    new Vegetable(
      'Myrtille',
      'Vaccinium',
      'Fruit d’une plante sauvage peu à peu cultivée, la myrtille évoque les confitures d’antan mais offre aussi, par ses saveurs fraîches, florales et boisées, un large éventail de possibilités culinaires.',
      [6, 9],
      '1 semaine au frais',
      '57 kcal pour 100g',
      'Riche en vitamine C',
      ['Non Renseigné'],
      ['Non Renseigné'],
      'Autant de noms qui désignent la délicieuse baie bleutée et qui attestent de l’importance de la myrtille dans l’environnement familier de nos ancêtres.',
      '../../../assets/vegetables/blueberry.png'
    ),

    new Vegetable(
      'Cassis',
      'Grossulariaceae',
      'Deux grandes variétés composent la majorité des cultures, concentrées dans quatre régions françaises. Ses fruits acidulés et juteux se consomment aussi bien nature que préparés en dessert ou sous forme de liqueur et de crème.',
      [8, 9],
      '24 à 48h dans le bac à légumes',
      '71 kcal pour 100g',
      'Riche en vitamine C',
      ['Non Renseigné'],
      ['Non Renseigné'],
      'Entre le XVIIIe et le XIXe siècle, le cassis était auréolé de vertus médicinales, réputé capable de vaincre toutes sortes de maux : fièvres, parasites, migraines… Sa réputation miraculeuse s’atténue au profit de sa culture, qui s’étend de plus en plus dès le milieu du XIXe siècle dans la région viticole de Bourgogne, puis à tout le reste de la France. La Côte d’Or reste le fief de la petite baie sombre, dont on tire une liqueur(1) et une crème célèbre aux quatre coins du monde.',
      '../../../assets/vegetables/blackcurrant.png'
    ),
    new Vegetable(
      'Avocat',
      'Lauraceæ',
      'Facile à consommer, il est présent sur les étals toute l’année pour contenter votre gourmandise et favoriser votre bien-être.',
      [10, 4],
      'Jusqu’à 5 jours à l’air ambiant.',
      '205 kcal pour 100g',
      'Riche en vitamine B9',
      ['Non renseigné'],
      ['Non renseigné'],
      'Originaire de Chine, l’avocatier poussait à l’état sauvage il y a 5 000 ans. Il fut introduit en Occident par Alexandre le Grand qui emprunta la célèbre Route de la Soie. Les Arabes le propagèrent par la suite dans tout le bassin méditerranéen, notamment dans le sud de l’Espagne. Le climat, particulièrement favorable, permit à l’avocatier de s’implanter durablement.' +
      'Un accueil mitigé en Europe' +
      'Ce fruit aujourd’hui apprécié des consommateurs eut quelques difficultés à s’implanter sur le territoire français. Importé d’Italie au milieu du XVe siècle, l’avocatier fit son entrée dans le potager de Versailles grâce à La Quintinie. Ce n’est que 300 ans plus tard que la culture de l’avocat se développa. Dans son Traité des arbres fruitiers, Duhamel du Monceau dénombre ainsi treize espèces d’avocats.',
      '../../../assets/vegetables/avocado.png'
    ),

    new Vegetable(
      'Potiron',
      'Cucurbitaceae',
      'Très présent dans les jardins d’antan, il redevient populaire depuis plusieurs années. Ses couleurs, sa forme et ses saveurs se marient à merveille avec tous les plats, traditionnels ou audacieux. Les enfants l’adorent.',
      [10, 1],
      null,
      '30 kcal pour 100g',
      'Riche en vitamine B5',
      ['Ail', 'Blette', 'Betterave', 'Oignon'],
      ['Brocoli', 'Chou', 'Chou de Bruxelles', 'Chou-fleur', 'Concombre', 'Cornichon', 'Courgette', 'Melon', 'Pomme de terre', 'Radis', 'Tomate'],
      'Le potiron a été introduit en Europe par les Portugais au XVIe siècle. Ils l’ont également fait découvrir en Chine.' +
      'À cette époque on ne distinguait pas les différentes courges. Ce n’est qu’en 1860, que le botaniste Charles Naudin établit une liste de vingt variétés de potirons.',
      '../../../assets/vegetables/winter-squash.png'
    ),

    new Vegetable(
      'Lentilles',
      'Fabaceae',
      'Bien adaptées aux sols pauvres, les nombreuses variétés se déclinent en plusieurs couleurs. Servie en entrée, en soupe ou en accompagnement, la lentille vous aide à garder la forme grâce à ses qualités nutritionnelles.',
      [1, 12],
      '1 à 2 jours dans le bac à légumes',
      '112 kcal pour 100g',
      'Riche en vitamine B6',
      ['Non renseigné'],
      ['Non renseigné'],
      'Originaire d’Asie centrale, la lentille est cultivée depuis l’aube de l’Humanité.' +
      'Elle s’est très vite répandue aux quatre coins de l’Europe grâce au développement des voies de communication sous l’Empire romain.' +
      'Encore blonde, à son arrivée en Europe, il ne lui a fallu que deux siècles pour prendre des pigments bleus et se teinter en vert.' +
      'Cultivée partout dans le monde au XXIe siècle, on ne trouve quasiment plus la lentille à l’état sauvage.',
      '../../../assets/vegetables/lentil.png'
    ),

    new Vegetable(
      'Petit pois',
      'Fabaceae',
      'Traditionnellement consommé sec, il a vu les techniques de production évoluer et se décline aujourd’hui en d’innombrables variétés aux qualités diverses. Ses qualités nutritionnelles originales en font un aliment de plaisir et de bien-être.',
      [5, 6],
      '2 jours max. dans le bac à légumes',
      '80 kcal pour 100g',
      'Riche en fibres et vitamine B9',
      ['Non renseigné'],
      ['Non renseigné'],
      'Le petit pois, aussi appelé pois potager, est l’un des plus vieux légumes cultivés en Europe et en Asie. En Iran, en Palestine, en Grèce ou encore en Suisse, le petit pois était déjà présent il y a 10 000 ans.' +
      'Il était alors consommé sec, on le concassait avant de le cuire.' +
      'Sa consommation fraîche est relativement récente. Il s’est implanté en France au XVIIe siècle en passant par l’Italie et les Pays-Bas. Il se développa rapidement autour de Paris.' +
      'Louis XIV adorait le petit pois, il demanda alors à son jardinier, La Quintinie, d’en faire la culture sous les serres à Versailles.' +
      'Le moine autrichien Mendel s’est servi du petit pois pour établir les premières lois de la génétique. Pendant 18 années, il a réalisé des croisements dans son potager et à constaté les variations de dimension, de forme et de couleur.' +
      'Si quelques dizaines de variétés étaient recensées au XVIe siècle, on dénombrait déjà 360 appellations en 1925. Ce chiffre n’a pas cessé de croître.',
      '../../../assets/vegetables/little-pea.png'
    ),

    new Vegetable(
      'Mâche',
      'Valerianaceae',
      'Ces rosettes de feuilles rondes, d’un beau vert foncé et brillant, sont charnues et croquantes. Originaire du bassin méditerranéen, la mâche fait figure de salade hivernale de référence sur les étals, même si elle se consomme tout au long de l’année. Son goût subtil et raffiné vous régale lorsqu’elle est crue, en salade, mais aussi cuite, en velouté et en accompagnement.',
      [10, 4],
      '2 ou 3 jours au réfrigérateur',
      '16 kcal pour 100g',
      'Riche en vitamine K1',
      ['Blette', 'Céleri', 'Chou', 'Chou de Bruxelles', 'Chou-fleur', 'Fraise', 'Haricot', 'Laitue', 'Oignon', 'Poireau'],
      ['Aucun'],
      'On retrouve la mâche représentée sur les tombeaux égyptiens, dès le Ve siècle avant notre ère.' +
      'En France, elle est cultivée depuis le XVIe siècle, déjà sur les bords de Loire.' +
      'Malgré son goût subtil, elle reste longtemps une nourriture paysanne et rustique.' +
      'Sélectionnée au fil des siècles, sa rosette est plus touffue, ses feuilles plus larges et plus tendres que celles de la mâche sauvage.' +
      'Il faut attendre le Second Empire pour que les restaurateurs et les gastronomes mettent cette petite salade au menu.',
      '../../../assets/vegetables/lambs-lettuce.png'
    ),

    new Vegetable(
      'Endive',
      'Asteraceae',
      'Sa culture nécessite un savoir-faire pointu, grâce auquel l’endive (ainsi qu’on l’appelle partout ailleurs) vous apporte ses bienfaits nutritionnels.',
      [10, 4],
      '6 jours dans le bac à légume du réfrigérateur',
      '20 kcal pour 100g',
      'Riche en vitamine B9',
      ['Carotte', 'Navet', 'Poireau', 'Radis'],
      ['Artichaut', 'Estragon', 'Laitue', 'Salsifis', 'Topinambour'],
      'L’endive est issue d’une chicorée  sauvage aux racines persistantes.' +
      'C’est un légume neuf ! Sa culture n’a été mise au point qu’en 1850 par le jardinier-chef de la Société d’Horticulture belge, un certain M. Bréziers. En cave et pendant l’hiver, il serre de petits monticules de terre autour de quelques pieds de chicorée sauvage. Quelques semaines plus tard, il obtient une salade en forme de fuseau, avec de larges feuilles pâles : la chicorée dite  witloof (ou feuille blanche en flamand) est née !' +
      'C’est trente ans plus tard que Paris découvre cette chicorée witloof. On raconte que le crieur des Halles, interrogé sur le nom de ce nouveau légume, fut pris de court, mais répondit avec aplomb : « endives de Bruxelles ». Le nom lui resta… sans la capitale.' +
      'Tout au long du XXe siècle, la sélection des plants permet d’obtenir de nouvelles variétés possédant moins d’amertume.' +
      'C’est surtout après la Seconde Guerre mondiale que l’endive conquiert d’autres étals que ceux de Belgique et du nord de la France.' +
      'Les techniques actuelles, et notamment le forçage (permettant la pousse en dehors de la saison normale de croissance), permettent à l’endive d’être disponible toute l’année.',
      '../../../assets/vegetables/chicory.png'
    ),

    new Vegetable(
      'Chicorée',
      'Asteraceae',
      'Dans ce cas, vous connaissez très certainement les chicorées, ces salades au goût amer ! En France, on trouve surtout des scaroles, des frisées et des endives.',
      [10, 2],
      '2 jours dans le bac à légumes',
      '13 kcal pour 100g',
      'Riche en vitamine B9',
      ['Carotte', 'Endive', 'Haricot', 'Laitue', 'Radis'],
      ['Fenouil', 'Lavande'],
      'On consomme des chicorées depuis très longtemps déjà. En France, les scaroles et les frisées sont dégustées en salades depuis l’Antiquité. Les chicorées amères, elles, ont toujours été appréciées pour leurs vertus médicinales.' +
      'En France, la production tend à diminuer depuis les années 90, en raison notamment de la concurrence espagnole.',
      '../../../assets/vegetables/curly-endive.png'
    ),

    new Vegetable(
      'Pastèque',
      'Cucurbitaceæ',
      'Sa chair sucrée et parfumée est disponible sur les étals français toute l’année. Ce fruit est délicieux nature, gratiné ou assaisonné. Sa richesse en vitamines combinée à sa légèreté en font un fruit idéal au quotidien, qui vous apportera tonus et vitalité !',
      [6, 8],
      '1 semaine à l’air ambiant',
      '38 kcal pour 100g',
      'Riche en vitamine C',
      ['Non renseigné'],
      ['Non renseigné'],
      'Avec cette invention, les Japonais ont élevé le gain de place et le stockage au rang d’œuvre d’art. Des agriculteurs dans le sud de l’archipel ont ainsi eu l’idée de cultiver des pastèques carrées, facilement empilables. Comment ? En faisant pousser le fruit dans un bocal de verre. Au fur et à mesure de sa croissance, la pastèque épouse naturellement la forme du récipient. Une curiosité qui ne modifie en rien la qualité du fruit, mais qui double son prix de vente !Arbre robuste et fécond, le pastèquer s’est développé en Asie, en Afrique et en Amérique. Sur ce continent, le fruit était utilisé depuis longtemps par les peuples du Sud et les Indiens. Il servait en effet à attendrir les viandes un peu fermes, en râpant la pulpe dessus avant de recouvrir le mélange des feuilles de pastèquer.' +
      'Connue en Europe depuis quatre siècles, la pastèque reste encore une curiosité sur les étals français.',
      '../../../assets/vegetables/watermelon.png'
    ),

    new Vegetable(
      'Fleurs comestibles',
      null,
      'Naturelles, colorées et festives, les fleurs comestibles investissent nos assiettes et les embellissent. Alors profitez de la tendance : elles donnent de nouvelles idées recettes aux gourmands et aux cordons bleus, tout en participant à votre bien-être grâce à leurs qualités nutritives.',
      [3, 6],
      'Quelques heures à 2 jours dans le bac à légumes',
      '0 kcal pour 100g',
      null,
      ['Non renseigné'],
      ['Non renseigné'],
      'Le retour à une cuisine fleurie' +
      'Les Grecs et les Romains utilisaient déjà la bourrache. Les Grecs la nommaient « euphrosynon » («  joie du festin »), car ses fleurs macérées dans du vin étaient censées rendre joyeux qui la buvaient.' +
      'Au Moyen-Age, on trouvait la bourrache dans tous les jardins, pour un usage médicinal plus qu’alimentaire.' +
      'Autrefois, on consommait les fleurs soit fraîches dans les salades, soit en infusion, soit confites dans le sucre. Puis, au XXe siècle, ce type d’usages décline. Mais depuis quelques années, la tendance s’inverse. Les fleurs, un temps délaissées, reviennent en force dans la cuisine et s’invitent dans l’assiette.' +
      'Désormais, les fleurs comestibles se rencontrent régulièrement dans les plats élaborés des restaurants gastronomiques.',
      '../../../assets/vegetables/flower.png'
    ),

    new Vegetable(
      'Sapotille',
      'Sapotaceæ',
      'Son arbre produit quantité de fruits toute l’année, principalement sous le soleil de l’Inde et du Mexique. Sa pulpe est riche en saveurs, et contient quantités de vitamines et de minéraux essentiels à votre bien-être.',
      [1, 12],
      'Dans un endroit frais et aéré',
      '80 kcal pour 100g',
      'Riche en vitamine C',
      ['Non renseigné'],
      ['Non renseigné'],
      'La sapotille apparaît pour la première fois dans l’Odyssée d’Homère. L’auteur y raconte l’arrivée d’Ulysse et de ses compagnons chez les Lotophages, un peuple mythologique. Ces derniers leur font goûter le fruit dont ils se nourrissaient : le lotos, qui n’est autre que la sapotille.' +
      'Le fruit s’épanouit sur les rivages chauds de la Méditerranée. Il constitue ainsi, selon Théophraste, la nourriture exclusive des armées pendant les guerres puniques entre l’Empire romain et Carthage (Ve-IVe siècles av. J.-C.).' +
      'Au Moyen-Âge, la sapotille est utilisée par les médecins arabes comme ingrédient de base de sirops pour soigner rhumes, toux et enrouements.' +
      'Introduite au XVe siècle dans les Antilles, le fruit n’est découvert par les Européens qu’au XVIIIe siècle.',
      '../../../assets/vegetables/sapodilla.png'
    ),

    new Vegetable(
      'Pitaya',
      'Cactaceæ',
      'Fruit exotique né du cactus, elle s’apprécie toute l’année, apportant aux étals sa couleur éclatante. La pitaya se déguste crue : elle est très rafraîchissante et sa saveur est fine, douce et parfumée. Riche en vitamines, elle apporte énergie et vitalité à chaque bouchée !',
      [1, 12],
      'Quelques heures après la coupe',
      '36 kcal pour 100g',
      'Riche en vitamine C',
      ['Non renseigné'],
      ['Non renseigné'],
      'Les colons français décident en effet d’en importer quelques plants au Vietnam. Le « fruit du dragon » y est rapidement cultivé, mais réservé, dans les premiers temps, exclusivement à la famille royale et à quelques familles privilégiées.' +
      'Les exploitations de pitayas se développent peu à peu et le fruit devient un des principaux produits d’exportation du Vietnam avec le durian, fruit odorant très apprécié de la population locale.' +
      'Suite à ce succès, la culture de la pitaya s’étend à toute l’Asie du Sud-Est, qui adopte rapidement ce fruit décoratif et goûteux.' +
      'Le « fruit du dragon » colore les étals français depuis moins de 20 ans. Sa consommation reste encore marginale même si une faible quantité est produite sur l’île de la Réunion.',
      '../../../assets/vegetables/strawberry-pear.png'
    ),

    new Vegetable(
      'Mangue',
      'Anacardiaceae',
      'Sa chair, d’un beau jaune-orangé, a un délicat goût sucré. Avec plusieurs milliers de variétés, la mangue vous offre une diversité peu commune de couleurs et de saveurs. Aliment bien-être, elle allie plaisir gourmand et apport nutritionnel équilibré.',
      [4, 7],
      '1 jour à l’air ambiant',
      '73 kcal pour 100g',
      'Riche en vitamine B9 et C',
      ['Non renseigné'],
      ['Non renseigné'],
      'Cultivé depuis plus de 4 000 ans, le manguier s’est rapidement diffusé dans le monde entier : dès le XVIe siècle, les Arabes l’introduisent en Afrique, et les Portugais l’implantent en Amérique centrale et en Amérique du Sud' +
      'Aujourd’hui, la mangue est cultivée dans tous les pays tropicaux du globe. On en connaît plusieurs centaines d’espèces différentes, dont quelques-unes seulement sont commercialisées.' +
      'Connue depuis longtemps au Royaume-Uni à travers des plats d’inspiration indienne, la mangue n’apparaît que tardivement sur les tables françaises, où elle reste longtemps cantonnée aux repas de fêtes. Rare et chère, la mangue était encore considérée comme un produit de luxe dans les années 1970.' +
      'Auparavant, la mangue n’était utilisée qu’en dessert. Arrivée voici 25 ans avec la vague de la « nouvelle cuisine », le fruit est entré dans des préparations salées. Il s’accommode désormais aussi bien à la chinoise, au wok ou compotée dans les chutneys.',
      '../../../assets/vegetables/mango.png'
    ),

    new Vegetable(
      'Papaye',
      'Caricaceæ',
      'Sa chair sucrée et parfumée est disponible sur les étals français toute l’année. Ce fruit est délicieux nature, gratiné ou assaisonné. Sa richesse en vitamines combinée à sa légèreté en font un fruit idéal au quotidien, qui vous apportera tonus et vitalité !',
      [10, 12],
      'À l’air ambiant',
      '42 kcal pour 100g',
      'Riche en vitamine C et B9',
      ['Non renseigné'],
      ['Non renseigné'],
      'La papaye trouve ses origines en Amérique tropicale. L’espèce s’est rapidement propagée, notamment en raison des caractéristiques de ses graines, qui conservent longtemps leur pouvoir germinatif. Elles ont ainsi pu supporter les longs voyages en mer ou sur terre qu’elles connurent à la découverte du Nouveau Monde. La papaïne extraite de la papaye verte est utilisée tant en brasserie qu’en pharmacie.' +
      'Arbre robuste et fécond, le papayer s’est développé en Asie, en Afrique et en Amérique. Sur ce continent, le fruit était utilisé depuis longtemps par les peuples du Sud et les Indiens. Il servait en effet à attendrir les viandes un peu fermes, en râpant la pulpe dessus avant de recouvrir le mélange des feuilles de papayer.' +
      'Connue en Europe depuis quatre siècles, la papaye reste encore une curiosité sur les étals français.',
      '../../../assets/vegetables/papaya.png'
    ),

    new Vegetable(
      'Physalis',
      'Solanaceæ',
      'Les surnoms donnés au physalis sont aussi nombreux que ses variétés. Ce fruit originaire des Andes appartient à la même famille que la tomate et l’aubergine. La baie, juteuse et sucrée ou acidulée, est recouverte d’un calice qui sèche à maturité.',
      [11, 2],
      '3 à 4 mois à l’air ambiant',
      '32 kcal pour 100g',
      'Riche en vitamine B',
      ['Non renseigné'],
      ['Non renseigné'],
      'Le fruit est également planté en France (principalement les espèces non comestibles et décoratives) et s’acclimate même à Paris. Ainsi, dès le XVIe siècle, les fleuristes de la capitale utilisent le physalis ornemental dans la confection de bouquets.' +
      'Des botanistes ont tenté d’acclimater le coqueret du Pérou sous nos latitudes dès le XVIIIe siècle, mais il reste une plante rare, objet de curiosité et non de consommation courante. Jusqu’au siècle des lumières et l’introduction d’une variété comestible en France, le physalis était considéré même comme vénéneux !',
      '../../../assets/vegetables/physalis.png'
    ),

    new Vegetable(
      'Goyave',
      'Myrtaceae',
      'La forme de la goyave évoque tantôt la poire, tantôt la pomme. Fruit fraîcheur et aliment tonique, la goyave présente une teneur exceptionnelle en vitamine C. Autre atout majeur : son apport énergétique particulièrement bas en fait un fruit facile à consommer par tous',
      [11, 12],
      '2 jours dans le bac à légumes',
      '68 kcal pour 100g',
      'Riche en vitamine C',
      ['Non renseigné'],
      ['Non renseigné'],
      'Les grains du goyavier restent intactes dans les intestins et donnent une particularité au goyavier : sa propagation est ainsi assurée par les oiseaux.' +
      'L’arbre possède des feuilles aromatiques, souvent utilisées comme infusions digestives.' +
      'Dans les îles du Pacifique, la pulpe de goyave est très utilisée par les femmes qui la transforment en un soin hydratant et régénérant pour la peau du visage et du corps.',
      '../../../assets/vegetables/guava.png'
    ),

    new Vegetable(
      'Grenade',
      'Punicacae',
      'Aliment plaisir grâce à son parfum subtil, la grenade donne un agréable petit brin de fantaisie et d’exotisme. Ses couleurs vives apportent un rayon de soleil dans vos assiettes et ses apports nutritifs participent à notre bien-être.',
      [11, 2],
      '10 jours à l\'air libre',
      '80 kcal pour 100 g',
      'Riche en vitamine C',
      ['Non renseigné'],
      ['Non renseigné'],
      'Le jus savoureux et désaltérant de la grenade était très apprécié des caravanes commerciales traversant le désert.' +
      'Arrivée avec les conquistadores espagnols, la grenade s’est implantée dans les îles Caraïbes et en Amérique latine.' +
      'Entre histoire et mythologie.' +
      'Mentionnée dans la Bible et dans le Coran, la grenade est depuis des temps immémoriaux symbole de vie et de fertilité, probablement en raison de ses très nombreuses graines.',
      '../../../assets/vegetables/pomegranate.png'
    ),

    new Vegetable(
      'Kaki',
      'Ebenaceae',
      'Sa chair est fondante ou croquante, mais toujours parfumée et sucrée. Fruit festif et dépaysant, il est fragile, et mérite toutes les attentions. Ses apports nutritionnels incitent eux aussi à vous en régaler.',
      [10, 1],
      '1 à 2 jours à l’air ambiant',
      '67 kcal pour 100g',
      'Riche en vitamine C',
      ['Non renseigné'],
      ['Non renseigné'],
      'Fruit du plaqueminier, le kaki tire son nom de la langue japonaise. Sa consommation en Europe se développe à la fin du XIXe siècle, profitant notamment de la mode du japonisme et des estampes. En 1873, les premiers kakis arrivent à Paris.' +
      'Introduit à la même époque à Toulon, le fruit connaît rapidement un grand succès en Provence. Les variétés du Canet et de Marseille sont très renommées.',
      '../../../assets/vegetables/persimmon.png'
    ),

    new Vegetable(
      'Litchi',
      'Sapindaceae',
      'Fruit rafraîchissant et exotique, son succès est grandissant sur les étals français. Traditionnellement associé au Nouvel an chinois, le litchi est un fruit festif qui regorge de vitamines.',
      [11, 1],
      '2 jours à l’air ambiant',
      '81 kcal pour 100g',
      'Riche en vitamines C et B9',
      ['Non renseigné'],
      ['Non renseigné'],
      'Le mot « litchi » ou « letchi » apparaît en France en 1721. Déformation du chinois « li-chi », via le portugais « letchia », son nom est le reflet des premiers grands échanges commerciaux maritimes avec l’Empire du Milieu.' +
      'Dès 1724, la France introduit ce fruit sur les îles de La Réunion et de Madagascar, où il fait l’objet d’une culture importante.' +
      'Un fruit festif.' +
      'Dans son pays d’origine et dans tout l’hémisphère nord, la saison des litchis correspond aux mois de juin et juillet ; alors que dans les pays de l’hémisphère sud, les récoltes se tiennent en décembre et janvier. Le fruit se trouve ainsi fortement associé aux fêtes de Noël et du Nouvel An.',
      '../../../assets/vegetables/lychee.png'
    ),

    new Vegetable(
      'Framboise',
      'Rosaceae',
      'La framboise se déguste nature ou cuisinée, mais reste fragile. Ce petit fruit rouge présente un apport calorique modéré : avec elle, alliez plaisir en bouche et bien-être !',
      [5, 10],
      '2 jours max. dans le bac à légumes',
      '49,2 kcal pour 100g',
      'Riche en vitamine C et B9',
      ['Ail', 'Myosotis', 'Souci', 'Tanaisie'],
      ['Pomme de terre'],
      'Entre histoire et légende: En réalité, le framboisier est originaire des zones montagneuses d’Europe occidentale, où il existe toujours à l’état sauvage. Lors de vos prochaines vacances à la montagne, vous aurez peut-être la chance de trouver des framboises dans les sous-bois. Le framboisier sauvage est en effet toujours présent dans les Alpes, les monts d’Auvergne et les Vosges. 19ème siècle : la framboise devient un fruit de bouche. A la Renaissance, la framboise fait peu à peu son entrée dans les jardins. Sélectionnée depuis plusieurs siècles afin de la rendre plus robuste et plus prolifique, elle se bonifie au fil du temps. En France, on ne croque cependant cette savoureuse baie qu’au 19ème  siècle. Elle était jusque-là surtout cultivée pour son parfum et ses vertus médicinales, ainsi que pour la fabrication de boissons. La culture commerciale de la framboise se développe à partir des années 1950.',
      '../../../assets/vegetables/raspberry.png'
    ),

    new Vegetable(
      'Oseille',
      'Polygonaceae',
      'Presque impertinente, elle prend le palais à rebrousse-papilles avec son goût frais, pétillant et acidulé. Laissez-vous surprendre !',
      [5, 10],
      '3 jours max. dans le bac à légumes',
      '23 kcal pour 100g',
      'Riche en vitamine C',
      ['Non Renseigné'],
      ['Non Renseigné'],
      'Ce n’est qu’au Moyen Âge que l’Homme a commencé à cultiver l’oseille. Elle est rapidement devenue un aliment de base de la cuisine française, en entrant notamment dans la composition des fameuses sauces vertes, servies à la cour avec le grand gibier. Ses vertus médicinales et culinaires lui valurent tous les honneurs au XVIIe siècle. Il fallut attendre la gourmande Catherine de Médicis pour qu’il soit reconnu et cultivé. De nos jours encore, on nomme « à la florentine », nombre de mets à base d’épinards.',
      '../../../assets/vegetables/sorrel.png'
    ),

    new Vegetable(
      'Ail',
      'Liliaceæ',
      'Sa culture, très simple, ne demande pas d’attention particulière et les espèces produites en France proposent un large éventail de goûts. En cuisine, ce condiment relève et sublime les saveurs, tout en participant à votre bien-être grâce à ses atouts nutritionnels.',
      [8, 12],
      '6 mois à 1 an à l’abri de la lumière',
      '130 kcal pour 100g',
      'Riche en vitamine B6',
      ['Chou', 'Tomates','Blette', 'Betterave', 'Camomille', 'Carotte', 'Céleri', 'Concombre', 'Cornichon', 'Epinard', 'Fraise', 'Framboise', 'Laitue', 'Panais','Rutabaga'],
      ['Asperge', 'Brocoli', 'Chou', 'Chou de Bruxelles', 'Chou-fleur', 'Ciboulette', 'Echalote', 'Fève', 'Haricot', 'Navet', 'Oignon', 'Piment', 'Poireau', 'Pois', 'Poivron', 'Pomme de terre', 'Sauge','Tomate'],
      'Un délice de tous les jours: Les Grecs et les Romains en font même leur petit-déjeuner. Ce dernier se compose d’une belle tranche de pain relevé d’une gousse d’ail longuement frottée sur sa mie. Le peuple gaulois se montre également très friand de ce condiment, goût qui se perpétue au Moyen-Âge. La France commence alors à être un peuple cultivateur d’ail. Un condiment de base: onsidéré depuis longtemps comme l’un des ingrédients culinaires de base, l’ail est présent dans les cuisines aussi bien dans le Midi, son terroir de prédilection, que dans toutes les autres régions françaises. Le saviez-vous ? Certains grands personnages historiques avaient l’ail en horreur. Ainsi, au XIVe siècle, Alphonse, roi de Castille, fit publier un ordre qui intimait aux chevaliers ayant mangé de l’ail ou de l’oignon de rester en dehors de la cour. Ces derniers devaient également s’abstenir de converser avec d’autres chevaliers. De nombreuses croyances entourent l’ail. On lui impute ainsi la faculté de retrouver les âmes égarées, la protection contre le mauvais œil, les serpents, le danger, ou encore d’éloigner les vampires et le diable. En mer, l’ail éloignerait malchance, tempêtes et monstres marins… L’ail appartient à la famille botanique des liliacées, comme la ciboulette et l’oignon, mais aussi comme le lys et la tulipe !',
      '../../../assets/vegetables/garlic.png'
    ),

    new Vegetable(
      'Echalote',
      'Liliaceæ',
      'Sa culture est principalement située dans l’ouest de la France et en Bretagne. Présente sur les étals toute l’année, l’échalote dynamise toutes vos recettes au quotidien, tout en vous prodiguant ses bienfaits.',
      [10, 12],
      '1 ou 2 mois au sec, frais et ombre 2 à 3 jours dans le bac à légumes',
      '66 kcal pour 100g',
      'Riche en vitamine A, B1, B2, B3, B6, B9 et C',
      ['Blette', 'Betterave', 'Carotte', 'Concombre', 'Cornichon', 'Fraise', 'Laitue'],
      ['Ail', 'Asperge', 'Brocoli', 'Chou', 'Chou de Bruxelles', 'Chou-fleur', 'Ciboulette', 'Fève', 'Haricot', 'Oignon', 'Piment', 'Pois', 'Poivron', 'Pomme de terre', 'Tomate'],
      'C’est au Turkestan, région d’Asie bordée par la mer Caspienne que la première variété d’échalote voit le jour. Elle tient son nom de la ville d’Ascalon, en Judée. Les Perses et les Égyptiens adoptent vite le petit légume qu’ils considèrent comme une plante sacrée. Une culture ancienne: La croyance veut que les Croisés l’aient introduite chez nous, de retour de Terre Sainte. Mais des écrits officiels datant du temps de Charlemagne et dans lesquels elle figure réfutent cette hypothèse. Appelée alors ascalonia, l’échalote devint vite une culture à part entière en France. On la trouve ainsi dans les potagers familiaux ainsi que dans des exploitations agricoles au XIIe et XIIIe siècle. Des marchands ambulants viennent en proposer aux Parisiens. Longtemps produite dans le nord de la France, l’échalote est aujourd’hui principalement cultivée en Bretagne et dans le Val de Loire, où elle fut introduite au XVIIe siècle.',
      '../../../assets/vegetables/shallot.png'
    ),

    new Vegetable(
      'Menthe',
      'Lamiacées',
      'La menthe est une plante aromatique très facile à cultiver au jardin ou en pot. Ses feuilles riches en menthol s\'apprécient dans le thé ou en cuisine, et son abondant feuillage fait d\'elle une plante décorative, dans les massifs ou en couvre-sol. Il existe de très nombreuses espèces de menthes.',
      [3, 10],
      '1 semaine au réfrégirateur',
      '58 kcal pour 100g',
      'Vitamine B9',
      ['Non Renseigné'],
      ['Non Renseigné'],
      'On pensait la menthe aphrodisiaque dans l’Antiquité. Le Moyen Âge s’en servait beaucoup, en médecine comme en cuisine. Au XVIe siècle, les menthes étaient considérées comme des panacées. Aujourd’hui, elles ne sont plus utilisées que pour les troubles digestifs et la nervosité. Au XVIIe siècle, le botaniste anglais Ray reçut un échantillon de menthe différente dans les cultures de menthe douce. Cette variété fut nommée menthe poivrée, en allusion à son parfum si caractéristique. Elle est actuellement l’une des menthes les plus étudiées pour ses propriétés médicinales. Au XIXe siècle, elle était la gloire de Milly-la-Forêt où elle fut cultivée industriellement.',
      '../../../assets/vegetables/mint.png'
    ),

    new  Vegetable(
    'Asperge',
    'Asparagacae',
    'Elle se déguste simplement avec un panaché de vinaigrettes ou se cuisine en gratin, en velouté, etc. Longtemps considérée comme un produit de luxe, elle est aujourd’hui beaucoup plus abordable. ',
    [4, 6],
    '3 jours dans le bac à légumes',
    '18 kcal pour 100g (asperge blanche) et 26 kcal pour 100 g (asperge verte)',
    'Riche en vitamine B9 (asperge blanche) et Riche en vitamine B9 et K1 (asperge verte)',
    ['Artichaut', 'Concombre', 'Cornichon', 'Epinard', 'Laitue','Persil', 'Radis', 'Sauge'],
    ['Ail', 'Blette', 'Betterave','Echalote', 'Haricot', 'Oignon', 'piment', 'Poireau', 'Poivron'],
    'Probablement originaire du bassin méditerranéen, l’asperge est d’abord consommée – le plus souvent à l’état sauvage – chez les Égyptiens et les Grecs. Ce sont les Romains, plus tard, qui en développent la culture, mais ce légume reste alors réservé aux riches gastronomes.' +
    'Un peu oubliée ensuite, l’asperge ne réapparaît qu’à la Renaissance, où elle retrouve la faveur des gourmets. Produit de luxe, elle est particulièrement appréciée des rois et des princes. Henri III en sert à ses favoris, et Louis XIV l’exige sur sa table en toute saison. Pour satisfaire son désir, La Quintinie, le responsable des jardins royaux, met au point un système de culture sous abris.' +
    'Cependant, jusqu’au début du 19è siècle, seuls les amateurs fortunés peuvent s’offrir ce légume raffiné et fort cher. L’asperge commence seulement à se démocratiser, lorsque à cette époque, les cultures se répandent. D’abord en région parisienne (près d’Argenteuil, Bezons et Épinay, socle dont dérivent une bonne part des variétés contemporaines), puis dans les années 1870, dans le Val de Loire. Elle conquiert ensuite l’Aquitaine, la Provence et le Midi de la France, aujourd’hui autres grandes régions de production.',
    '../../../assets/vegetables/asparagus.png'
  ),

  new Vegetable(
    'Betterave',
    'Chenopodiaceæ',
    'Son goût très doux et sucré, délicat, est apprécié en crudités ou cuisiné et accommodé. Pensez à l’intégrer à vos menus hebdomadaires, dès qu’elle est présente sur les étals, pour bénéficier de toutes les vitamines et les nutriments qu’elle contient.',
    [10, 3],
    'Dans le bac à légumes',
    '42 kcal pour 100g',
    'Riche en vitamine B9',
    ['Brocoli', 'Chou', 'Chou-fleur', 'Laitue', 'Oignon', 'Pomme de terre', 'Sauge','Ail','Céleri', 'Chou de Bruxelles','Echalote','Radis'],
    ['Asperge', 'Blette', 'Carotte', 'Epinard', 'Fève',  'Haricot à rames', 'Maïs', 'Poireau', 'Pomme de terre', 'Tomate.'],
    'Toutes deux sont nées de l’évolution de la blette maritime, qui poussait à l’état sauvage sur les rivages de la Méditerranée et de l’Atlantique.',
    '../../../assets/vegetables/beetroot.png'
  ),

  new Vegetable(
    'Céleri-rave',
    'Apiaceæ',
    'Ce légume rustique s’est développé et a imposé sa présence sur les étals grâce à sa chair savoureuse, croquante quand elle est crue, fondante après cuisson. Ce beau céleri tout en rondeur est très concentré en vitamines et minéraux essentiels, tout en étant très peu énergétique. Invitez-le régulièrement dans vos assiettes pour savourer tous les bienfaits de sa chair délicate.',
    [10, 3],
    '6 jours max. dans le bac à légumes',
    '29 kcal pour 100g',
    'Riche en vitamine B9',
    ['Ail', 'Blette', 'Betterave', 'Brocoli', 'Chou', 'Chou de Bruxelles', 'Chou-fleur', 'Ciboulette',  'Concombre', 'Cornichon','Courgette', 'Epinard', 'Fève', 'Haricot', 'Mâche', 'Oignon', 'Poireau', 'pois', 'Radis', 'Raifort', 'Rutabaga',  'Tomate'],
    ['Pomme de terre','Carotte', 'Laitue', 'Maïs', 'Panais', 'Persil',],
    'On lui prête alors tous types de pouvoirs presque « magiques » : guérison de la mélancolie, choix du sexe d’un enfant à naître, lutte contre les maux de dents, etc.' +
    'Ce n’est qu’à la Renaissance qu’il intègre les potagers. Son amélioration s’est faite de manière empirique dans les régions de production qui ont donné leur nom aux variétés anciennes : de Gennevilliers, de Rueil, de Prague ou d’Erfurt. Mais il faudra attendre le milieu du XIXe siècle pour que le céleri s’invite définitivement dans les cuisines et les assiettes.',
    '../../../assets/vegetables/celery-root.png'
  ),

  new Vegetable(
    'Navet',
    'Brassicaceae',
    'Originaire d’Europe de l’Est, cette racine gorgée d’eau n’est pas aussi fade que sa réputation le laisse entendre. Au contraire, son goût caractéristique donne une touche indispensable à de nombreuses spécialités régionales comme à des plats très raffinés. Ses qualités nutritionnelles en font un compagnon de votre bien-être, apprécié de l’automne au printemps.',
    [10, 5],
    '3 ou 4 jours dans le bac à légumes',
    '21 kcal pour 100g',
    'Riche en vitamine C et B9',
    ['Brocoli', 'Camomille', 'Chou', 'Chou de Bruxelles', 'Chou-fleur', 'Navet', 'Pois', 'Radis', 'Tomate'],
    ['Ail', 'Brocoli', 'Chou', 'Chou de Bruxelles', 'Chou-fleur', 'Oignon', 'Pomme de terre', 'Rutabaga'],
    'Originaire d’Europe, le navet est également présent en Inde depuis des siècles.' +
    'Des fouilles archéologiques ont permis de comprendre que le navet était autrefois cuit sous la cendre.' +
    'Jusqu’au XVIIIe siècle, il était consommé au quotidien dans toute l’Europe.' +
    'Il était surnommé “légume de disette” ou “légume des pauvres”.' +
    'Il a progressivement perdu sa place de choix avec l’arrivée de la pomme de terre.',
    '../../../assets/vegetables/turnip.png'
  ),

  new Vegetable(
    'Courgette',
    'Cucurbitaceæ',
    'Ce légume du soleil qui fleure bon la Méditerranée est présent dans les assiettes tout l’été. Facile à préparer, la courgette se consomme sous toutes ses formes. Et sa richesse en vitamines vous donne de l’énergie à la belle saison.',
    [5, 9],
    '4 ou 5 jours dans un endroit frais et sec',
    '23 kcal pour 100g',
    'Riche en vitamine C',
    ['Basilic', 'Bourrache', 'Capucine', 'Céleri', 'Fève', 'Haricot à rames', 'Maïs', 'Marjolaine', 'Menthe', 'Oignon', 'Pois'],
    ['Brocoli', 'Chou', 'Chou de Bruxelles', 'Chou-fleur', 'Concombre', 'Cornichon', 'Courgette', 'Melon', 'Pomme de terre', 'Radis', 'Tomate'],
    'À l’instar de nombreux autres fruits et légumes, la courge fut découverte par les Européens lorsqu’ils débarquèrent dans le Nouveau Monde. Ils en décrivirent très vite les différentes variétés, avant de les ramener pour les cultiver comme légume.' +
    'L’invention de la courgette' +
    'Cueillies à complète maturité, les courges permettaient de meilleurs rendements ainsi qu’une durée de conservation accrue par rapport aux courgettes que nous consommons actuellement. Ce sont les Italiens qui, au XVIIIe siècle, décidèrent de les déguster avant complète maturité. C’est ainsi que naquit… la courgette !',
    '../../../assets/vegetables/courgette.png'
  ),

  new Vegetable(
    'Pignon de pin',
    'Pinaceæ',
    'Ce petit fruit à coque est cultivé sur les rivages de la Méditerranée, dont elle apprécie la chaleur et le climat clément. Le goût généreux et savoureux du pignon de pin se marie facilement et parfume agréablement tous types de préparations, de l’entrée au dessert. Sa richesse en vitamines, oligo-éléments et nutriments essentiels en fait un concentré d’énergie à parsemer dans vos recettes.',
    [1, 12],
    'Dans un endroit sec et frais',
    '695 kcal pour 100g',
    'Riche en vitamine B2',
    ['Non renseigné'],
    ['Non renseigné'],
    'Consommé depuis des millénaires, le pignon de pin est apprécié par de nombreux peuples, principalement pour les vertus médicinales qu’on lui prête.' +
    'Au Moyen-Âge, il était salué pour ses bienfaits sur la santé, principalement sur les paralytiques et les impuissants. Il était aussi prescrit contre les toux. Mais le pignon de pin est également consommé comme une friandise, mélangé à des amandes, des pistaches et du sucre, ancêtre du nougat provençal.' +
    'Recette d’amour, la consommation de pignons de pin était recommandée aux jeunes amants au XVIIe siècle pour aider à la conception. Il est toujours prescrit pendant le siècle des Lumières comme remède contre la phtisie (forme de tuberculose). On lui prête alors les vertus de nettoyer les poumons et de soigner les ulcères.',
    '../../../assets/vegetables/pine-kernel.png'
  ),

  new Vegetable(
    'Rutabaga',
    'Brassicaceae',
    'Le rutabaga est un légume d’hiver peu à peu remis au goût du jour en cuisine. Il se déguste en crudité ou cuisiné.',
    [10, 4],
    '10 jours dans le bac à légumes',
    '39 kcal pour 100g',
    'Riche en vitamine C',
    ['Non renseigné'],
    ['Non renseigné'],
    'C’est l’Écosse qui popularise son utilisation dans l’alimentation humaine. Le rutabaga est l’un des ingrédients phare du plat national écossais, le fameux haggis (panse de brebis farcie).',
    '../../../assets/vegetables/swede.png'
  ),

  new Vegetable(
    'Panais',
    'Apiaceae',
    'Il renferme de très nombreux minéraux et vitamines. On peut le consommer cru comme cuit',
    [10, 4],
    '7 jours dans le bac à légumes',
    '90 kcal pour 100g',
    'Riche en vitamine B9',
    ['Ail', 'Brocoli',  'Chou', 'Chou de Bruxelles', 'Chou-fleur', 'Fève', 'Haricot', 'Navet', 'Oignon', 'Poireau', 'Pois', 'Radis'],
    ['Aneth', 'Blette', 'Carotte','Persil','Carottes', 'Céleri-branche','Céleri-rave'],
    'Les habitants du bassin méditerranéen ont commencé à consommer le panais bien longtemps avant notre ère. Mais Grecs et Romains ne semblaient guère l’apprécier, semble-t-il à cause de sa peau  résistante et de son cœur fibreux. Il sera progressivement domestiqué et amélioré à partir du VIe siècle.' +
    'Au Moyen-Age, nous dit le « capitulaire De Villis », il fait partie d’une centaine d’espèces communément cultivées dans les monastères. C’est à partir de là qu’il est progressivement distingué de la carotte dont il est cousin.',
    '../../../assets/vegetables/parsnip.png'
  ),

    new Vegetable(
      'Maïs',
      'Poaceae',
      'Le maïs, appelé blé d’Inde au Canada, est une plante herbacée tropicale annuelle de la famille des Poacées, largement cultivée comme céréale pour ses grains riches en amidon, mais aussi comme plante fourragère. Le terme désigne aussi le grain de maïs lui-même.',
      [4, 7],
      '1 semaine au réfrégirateur',
      '100 kcal pour 100g',
      'Vitamine B3, B5, B6, B9',
      ['Concombre', 'Haricot', 'Pois', 'Pomme de terre', 'Radis', 'Zucchini'],
      ['Tomate'],
      'L\'histoire du maïs commence par la culture de la téosinte il y a 9 000 ans au Mexique dans la haute vallée du Rio Balsas. À partir de -3000, on trouve du maïs dans toutes les basses terres de l\'Amérique centrale (Yucatan, Caraïbes, Andes). Les peuples mésoaméricains du centre du Mexique et du Yucatan en étaient très dépendants. Vers l\'an 1000, le peuple anasazi a probablement contribué à adapter le maïs aux zones tempérées et à créer le maïs corné. En Arizona, pays des « indiens Pueblos » (Hopis et Zunis), le maïs est alors considéré comme l\'enfant des dieux, symbole de vie. Les Nord-Amérindiens consommaient du maïs soufflé.',
      '../../../assets/vegetables/corn.png'
    ),

    new Vegetable(
      'Sauge',
      'Lamiaceae',
      'La sauge est un plante aux nombreuses espèces, utilisée en cuisine pour son arôme et en médecine pour ses vertus.',
      [10, 6],
      '1 semaine au réfrégirateur',
      '35 kcal pour 100g',
      'Riche en vitamine B9 et K',
      ['Asperge', 'Carotte', 'Chou pommé', 'Chou de Bruxelles', 'Chou-fleur', 'Fraise', 'Romarin', 'Tomate'],
      ['Ail', 'Brocoli', 'Concombre', 'Cornichon', 'Oignon', 'Poireau', 'Sarriette'],
      'Le sauge était une des plantes médicinales de l\'Antiquité et du Moyen Âge. Au xvie siècle, le botaniste Jacob Tabernae-Montanus raconte que les femmes égyptiennes avaient l\'habitude de boire du jus de sauge pour accroître leur fertilité, régulariser leurs cycles menstruels et faciliter leurs grossesses. Les Grecs appréciaient ses propriétés digestives et antiseptiques. Les Romains et les Arabes l\'employaient communément comme tonique et en compresse contre les morsures de serpent. Elle faisait partie des plantes dont la culture est recommandée dans les domaines royaux par Charlemagne dans le capitulaire De Villis. Cette herbe royale se répandit dans toute l\'Europe du Nord et de l\'Est grâce aux Bénédictins qui la cultivaient dans les jardins des monastères. Reconnue par les Chinois qui commerçaient avec les Hollandais au xviie siècle, ils n\'hésitaient pas à échanger leurs feuilles de thé les plus précieuses contre des feuilles de sauge. Louis XIV en avait même fait sa tisane d\'élection et en servait à tout propos. À la même époque, la sauge officinale fut acclimatée en Amérique où elle devint l\'herbe aromatique la plus populaire jusqu\'à la fin de la Seconde Guerre mondiale',
      '../../../assets/vegetables/sage.png'
    ),

    new Vegetable(
      'Raisin',
      '	Vitaceae',
      'Le raisin est un fruit éclatant de saveur. C’est un grand allié de la santé cardiovasculaire. Il est aussi une source de plusieurs vitamines et minéraux essentiels au bon fonctionnement de l’organisme. Le terme « raisin » est apparu dans la langue française en 1200 sous la forme première de « resin ». Il dérive du latin populaire racimus, qui signifie « grappe de baies ».',
      [11, 5],
      '5 jours dans le bac à légumes',
      '67 kcal pour 100g',
      'Vitamine B et C',
      ['Asperge', 'Hysope', 'Moutarde', 'Oeillet d’Inde', 'Ronce', 'Sauge'],
      ['Chou pommé', 'Chou de Bruxelles', 'Chou-fleur', 'Euphorbe', 'Laurier', 'Radis'],
      'L\'histoire du raisin remonte à 6000 ans avant J.C, puique l\'on en trouve trace en Europe Centrale : Arménie, Azerbaîdjan et Georgie. Dans l’antiquité, le raisin se répand d’abord dans le nord de l’Afrique, où sa culture s’étend le long du Nil, sur les bords de la Méditerranée, mais aussi en Europe du Nord, Suisse et Allemagne. Ce n’est qu’aux environs de 600 av. J.C que les Phocéens, en créant Massilia (Marseille), implantent la vigne en Gaule. Riche en symbole, il représente la vie dans les tombeaux des pharaons et devient la figure du culte de Dionysos dans la Grèce antique. La culture de la vigne est difficilement dissociable de l’histoire du vin, car il s’agit de son utilisation première. Longtemps utilisé à des fins viticoles, c\'est au 16 ème siècle qu\'il sera considéré pour d\'autres usages.',
      '../../../assets/vegetables/grape.png'
    ),

    new Vegetable(
      'Thym',
      'Lamiaceae',
      'Plante aromatique aux multiples propriétés, le thym est répandu sur les rocailles du sud de la France. C\'est une vivace connue pour son parfum caractéristique, son goût typé et ses petites feuilles couvertes de minuscules fleurs blanches ou roses.',
      [1, 12],
      'Séché, au printemps',
      '107 kcal pour 100g',
      'Vitamine C',
      ['Aubergine', 'brocoli', 'Chou pommé', 'Chou de Bruxelles', 'Chou-fleur', 'Fraise', 'Pomme de terre', 'Tomate'],
      ['Aucun'],
      'Originaire de l’Europe Méridionale, le thym permettait d’embaumer les défunts Égyptiens et Étrusques. D’ailleurs, à cette époque, « thymus » voulait dire « parfumer ». Une légende raconte qu’au cours de la Guerre de Troie, les larmes de la belle Hélène a permis la naissance de cet aromate. Utilisé alors en produits cosmétiques par les Romains, les Grecs quant à eux le brûlaient devant l’autel de leurs Dieux et devant les riches habitations. Plus curieux encore, les Romains en remplissez leurs couches le soir. Lors des croisades, les femmes brodaient à leurs hommes chevaliers des abeilles en thym pour leur donner courage. Ce condiment, placé sous l’oreiller, favoriserait la venue du sommeil. De nos jours, près de 300 variétés de thym sont répertoriées. Appelé aussi « farigoule » par les français du sud, cet aromate a des vertus médicinales et culinaires remarquables.',
      '../../../assets/vegetables/thyme.png'
    ),

    new Vegetable(
      'Aneth',
      'Apiaceae',
      'L’aneth est délicieuse avec son goût d’anis légèrement prononcé et accompagne parfaitement sauces, marinades et poissons.',
      [6, 9],
      '1 à 2 semaines au réfrégirateur, 1 mois au congélateur, séché',
      '43 kcal pour 100g',
      'Vitamine C',
      ['Pois', 'Carotte', 'Chicorée', 'Concombre', 'Endive', 'Laitue', 'Oignon'],
      ['Aucun'],
      'Originaire du bassin méditerranéen (Anethum graveolens) ou d\'Asie centrale (Anethum sowa), il était utilisé : par les anciens habitants de Judée et d’Israël comme épice à la période du Second Temple comme en atteste la mishna ; dans la médecine de l’Ayurveda où il est cité dans le Kashyapa Samhita ; par les Iraniens pour la cuisine ; par les Égyptiens il y plus de 5 000 ans, en tant que plante médicinale ; par les Grecs et les Romains pour son parfum, pour la cuisine et pour ses vertus médicinales. Il est mentionné dans l\'Évangile de Matthieu : « Malheur à vous, scribes et pharisiens hypocrites, qui payez la dîme de la menthe, de l\'aneth et du cumin, mais avez abandonné ce qu\'il y a de plus important dans la loi ! » Il fait partie des plantes dont la culture est recommandée dans les domaines royaux par Charlemagne dans le capitulaire De Villis (fin du viiie ou début du ixe siècle). En Angleterre, on le cultive depuis le xvie siècle.',
      '../../../assets/vegetables/dill.png'
    ),

    new Vegetable(
      'Ciboulette',
      'Liliaceae',
      'Une des fines herbes incontournables, au parfum plus nuancé que la ciboule et très facile de culture.',
      [4, 10],
      '1 semaine au réfrégirateur, séché',
      '30 kcal pour 100g',
      'Vitamine B6 et C',
      ['Carotte'],
      ['Aucun'],
      'Originaire de Chine, la ciboulette est une plante aromatique de la famille des alliacées, comme l\'ail ou l\'oignon. Utilisée depuis plus de 5000 ans en Asie, elle a été rapportée en Europe par Marco Polo. Le terme "ciboulette" est apparu dans la langue française au 14e siècle, et auparavant, il semblerait qu\'on l\'appelait ciboule. Elle était également surnommée "appétits", pour faire allusion à ses propriétés apéritives.',
      '../../../assets/vegetables/chives.png'
    )
  ];

  /**
   * Outputs the number of vegetables
   * @Return {number} Vegetable array size
   */
  getLength() {
    return this.vegetables.length;
  }

  sort(order: string) {
    function compare(a, b) {
      if (a.name < b.name) {
        return order === 'ascending' ? -1 : 1;
      }
      if (a.name > b.name) {
        return order === 'ascending' ? 1 : -1;
      }
      return 0;
    }

    this.vegetables.sort(compare);
  }

  /**
   * Outputs vegetable wanted.
   * @params {string} name
   */
  getVegetable(name: string) {
    const vegetable = this.vegetables.find(
      (s) => {
        return s.name === name;
      }
    );
    return vegetable != null ? vegetable : null;
  }

  /**
   * Get vegetables.
   * @Return {Object} - Vegetables array
   */
  getVegetables() {
    return this.vegetables;
  }


  /**
   * Get vegetables names.
   * @Return {Object} - Vegetables names array
   */
  getVegetablesNames(): { name: string }[] {
    return this.vegetables.map(vegetable => {
      const newVegetable = Object.assign({}, vegetable); // Copy the vegetable Object into newVegetable
      for (const i in vegetable) {
        if (i !== 'name') {
          delete newVegetable[i];
        }
      }
      return newVegetable;
    });
  }

  /**
   * Get vegetables size.
   * @Return {number} - Vegetables size
   */
  getVegetablesLen() {
    return this.vegetables.length;
  }
}
